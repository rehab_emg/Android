package com.imd4a.ntcust.rehabemg.test;

import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.github.anastr.speedviewlib.Gauge;
import com.github.anastr.speedviewlib.Speedometer;
import com.github.anastr.speedviewlib.TubeSpeedometer;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.imd4a.ntcust.rehabemg.R;

import java.util.ArrayList;
import java.util.Collections;

public class TestChartActivity extends AppCompatActivity {

    // 儀表板圖
    private TubeSpeedometer tubeSpeedometer;

    // 折線圖圖表
    private LineChart lineChart; // 折線圖表
    private ArrayList<Entry> entries = new ArrayList<>(); // 折線圖資料
    private LineDataSet lineDataSet; // 折線圖資料設定
    private LineData lineData; // 折線圖資料物件

    private XAxis xAxis; // 圖表X軸
    private YAxis yLeftAxis; // 圖表Y軸（左方）
    private YAxis yRightAxis; // 圖表Y軸（右方）

    private boolean isClick = false; // 測試用點及新增資料

    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_chart);

        btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(btnClick);

        lineChart = (LineChart) findViewById(R.id.chart);
        tubeSpeedometer = (TubeSpeedometer) findViewById(R.id.tubeSpeedometer);

        setTubeSpeedometer(); // 儀表圖設定
        setChart(); // 折線圖表設定
        setAxis(); // X、Y軸設定
        setLineData();

//        setData(200, 1000); // 新增數據

        Legend l = lineChart.getLegend(); // 取得圖例（數據設置後才能使用）
        l.setForm(LegendForm.LINE); // 修改圖例
        l.setEnabled(false);

        lineChart.invalidate(); // 刷新（更新）圖表
    }

    private void setTubeSpeedometer() {
        tubeSpeedometer.setMaxSpeed(100); // 最大值
        tubeSpeedometer.setUnitTextSize(0); // SpeedText 單位的文字大小
//        tubeSpeedometer.setUnitUnderSpeedText(true); // SpeedText 單位的文字於 SpeedText 下方
        tubeSpeedometer.setSpeedTextPosition(Speedometer.Position.BOTTOM_CENTER); // SpeedText 位置
        tubeSpeedometer.setLowSpeedPercent(40);
//        tubeSpeedometer.speedTo(60, 5000); // SpeedText 變動
        tubeSpeedometer.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
    }

    private void setChart() {
        lineChart.setDrawBorders(true); // 圖表邊界
        lineChart.setDrawGridBackground(true); // 圖表背景顏色，如果為false，setGridBackgroundColor失效
        lineChart.setGridBackgroundColor(Color.BLACK); // 背景顏色
        lineChart.getDescription().setEnabled(false); // 描述文字

        lineChart.setOnChartValueSelectedListener(lineChartOnChartValueSelected); // 圖表數值Selected監聽事件

        lineChart.setTouchEnabled(true); // 啟動圖表手勢（縮放、拖動等等）
        lineChart.setDragEnabled(true); // 圖表拖動
        lineChart.setScaleEnabled(true); // 圖表縮放
        lineChart.setPinchZoom(false); // 如果為false，則可分別在X軸或Y軸上進行縮放

//        lineChart.setBackgroundColor(Color.RED); // 替代背景顏色

        lineChart.invalidate(); // 刷新（更新）圖表
    }

    private void setAxis() {
        // X軸
        xAxis = lineChart.getXAxis(); // 取得圖表之X軸
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); // 設定位置為圖表下方（預設上方）
        xAxis.setAvoidFirstLastClipping(true); // 避免第一與最後的項目被圖表裁減掉
        xAxis.setAxisMinimum(0f); // 起始值
        xAxis.setAxisMaximum(100f);
        xAxis.setEnabled(false);

        // Y軸
        yLeftAxis = lineChart.getAxisLeft(); // 取得圖表之Y軸（左方）
        yLeftAxis.setInverted(false); // Y軸方向設定（如果為true，則最小值在上方，最大值在下方）
        yLeftAxis.setAxisMinimum(0f); // 起始值
        yLeftAxis.setAxisMaximum(1000f); // 最大值
        yLeftAxis.setEnabled(false);

        yRightAxis = lineChart.getAxisRight(); // 取得圖表之Y軸（右方）
        yRightAxis.setEnabled(false); // 是否啟用
    }

    private void setLineData() {
        lineDataSet = new LineDataSet(null, "sEMG Data");
        lineDataSet.setLineWidth(1.5f); // 折線圖線條寬度
        lineDataSet.setDrawCircles(false); // 折線圖圓點啟用
//        lineDataSet.setCircleRadius(4f); // 折線圖資料圓點半徑
//        lineDataSet.setCircleColor(Color.RED);
        lineDataSet.setColor(Color.WHITE);
        lineDataSet.setHighLightColor(Color.GREEN);

        lineData = new LineData();
        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    private OnChartValueSelectedListener lineChartOnChartValueSelected = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, Highlight h) {

        }

        @Override
        public void onNothingSelected() {

        }
    };

    private void setData(int count, float range) {
        for (int i = 0; i < count; i++) { // 假資料
            addData();
        }
    }

    private View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            addData();
            isClick = !isClick;
            loop();
        }
    };

    private void loop() {
        if (isClick) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    addData();
                    loop();
                }
            }, 50);
        }
    }

    private void addData() {
        if (lineDataSet.getEntryCount() == 0) {
            lineData.addDataSet(lineDataSet);
        }
        lineChart.setData(lineData);

        float xVal = lineDataSet.getEntryCount();
        float yVal = (float) (Math.random() * 1000);

        // tubeSpeedometer
        tubeSpeedometer.speedTo(yVal / 10);

        Entry entry = new Entry(xVal, yVal);
        Log.e("X數值", String.valueOf(xVal));
        lineData.addEntry(entry, 0);

        lineData.notifyDataChanged();
        lineChart.notifyDataSetChanged();

        lineChart.setData(lineData); // 將折線圖資料放入圖表

        if (xVal < 100) {
            xAxis.setAxisMinimum(0);
            xAxis.setAxisMaximum(100);
        } else {
            xAxis.setAxisMinimum(xVal - 99);
            xAxis.setAxisMaximum(xVal);
        }

        lineChart.invalidate(); // 刷新（更新）圖表
    }
}
