package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.imd4a.ntcust.rehabemg.LoginActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.MainClass.RegisterMetadata;
import com.imd4a.ntcust.rehabemg.Model.EMGDataDetailModel;
import com.imd4a.ntcust.rehabemg.Model.RegisterModel;
import com.imd4a.ntcust.rehabemg.ModifyActivity;
import com.imd4a.ntcust.rehabemg.R;
import com.imd4a.ntcust.rehabemg.RegisterActivity;
import com.imd4a.ntcust.rehabemg.WebApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserModifyFragment extends Fragment {

    private WebApi webApi = new WebApi();
    private String ChangeOtherUrl = webApi.GetChangeOtherApi();

    private RegisterMetadata registerMetadata = new RegisterMetadata(); // 註冊驗證資料

    private TextInputLayout TxtLayoutAccount, TxtLayoutName, TxtLayoutCellphone, TxtLayoutEmail;
    private EditText EdtAccount, EdtName, EdtCellphone, EdtEmail;
    private Button BtnSend;

    private boolean bAccount, bName, bCellphone, bEmail;

    public UserModifyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        View v = inflater.inflate(R.layout.fragment_analysis1_data, container, false);

        return inflater.inflate(R.layout.fragment_user_modify, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = ((ModifyActivity) getActivity()).bundle;
        initView();
        setData(bundle);
        setListener();
    }

    private void initView() {
        TxtLayoutAccount = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutAccount);
        TxtLayoutName = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutName);
        TxtLayoutCellphone = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutCellphone);
        TxtLayoutEmail = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutEmail);

        EdtAccount = (EditText) getActivity().findViewById(R.id.edtAccount);
        EdtName = (EditText) getActivity().findViewById(R.id.edtName);
        EdtCellphone = (EditText) getActivity().findViewById(R.id.edtCellphone);
        EdtEmail = (EditText) getActivity().findViewById(R.id.edtEmail);

        BtnSend = (Button) getActivity().findViewById(R.id.btnSend_user_modify);
    }

    private void setData(Bundle bundle) {
        EdtAccount.setText(bundle.getString("account"));
        EdtName.setText(bundle.getString("name"));
        EdtCellphone.setText(bundle.getString("cellphone"));
        EdtEmail.setText(bundle.getString("email"));
    }

    private void setListener() {
        // 送出資料
        BtnSend.setOnClickListener(BtnSendOnClick);

        // TextInputLayout 動態監聽
        EdtAccount.addTextChangedListener(new textListener(EdtAccount));
        EdtName.addTextChangedListener(new textListener(EdtName));
        EdtCellphone.addTextChangedListener(new textListener(EdtCellphone));
        EdtEmail.addTextChangedListener(new textListener(EdtEmail));
    }

    // 送出資料
    private View.OnClickListener BtnSendOnClick = new View.OnClickListener() {
        public void onClick(View view) {
//            EdtAccount.getText().toString();
//            EdtName.getText().toString();
//            EdtCellphone.getText().toString();
//            EdtEmail.getText().toString();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    ChangeOther();
                }
            }).start();
        }
    };

    //修改資料
    public void ChangeOther() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", EdtAccount.getText().toString());
            jsonObject.put("name", EdtName.getText().toString());
            jsonObject.put("cellphone", EdtCellphone.getText().toString());
            jsonObject.put("email", EdtEmail.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(ChangeOtherUrl)
                .post(body)
                .build();
        try {
            final Response response = client.newCall(request).execute();//response.getStatusLine().getStatusCode()
            if (response.isSuccessful()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(((ModifyActivity) getActivity()))
                                .setTitle("訊息")
                                .setMessage("修改成功 !")
                                .setCancelable(false)
                                .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }
                });
            }else{
                if(response.code() == 401){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(((ModifyActivity) getActivity()))
                                    .setTitle("訊息")
                                    .setMessage("修改失敗，資料格式輸入有誤 !")
                                    .setCancelable(false)
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // EditText動態監聽 驗證資料
    private class textListener implements TextWatcher {
        private View view;

        private textListener(View v) {
            this.view = v;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String s;

            // 資料驗證
            switch (view.getId()) {
                case R.id.edtAccount:
                    s = registerMetadata.AccountValidate(EdtAccount.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutAccount.setErrorEnabled(false);
                        bAccount = true;
                    } else {
                        TxtLayoutAccount.setErrorEnabled(true);
                        TxtLayoutAccount.setError(s);
                        bAccount = false;
                    }
                    break;
                case R.id.edtName:
                    s = registerMetadata.NameValidate(EdtName.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutName.setErrorEnabled(false);
                        bName = true;
                    } else {
                        TxtLayoutName.setErrorEnabled(true);
                        TxtLayoutName.setError(s);
                        bName = false;
                    }
                    break;
                case R.id.edtCellphone:
                    s = registerMetadata.CellphoneValidate(EdtCellphone.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutCellphone.setErrorEnabled(false);
                        bCellphone = true;
                    } else {
                        TxtLayoutCellphone.setErrorEnabled(true);
                        TxtLayoutCellphone.setError(s);
                        bCellphone = false;
                    }
                    break;
                case R.id.edtEmail:
                    s = registerMetadata.EmailValidate(EdtEmail.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutEmail.setErrorEnabled(false);
                        bEmail = true;
                    } else {
                        TxtLayoutEmail.setErrorEnabled(true);
                        TxtLayoutEmail.setError(s);
                        bEmail = false;
                    }
                    break;
            }
        }
    }
}
