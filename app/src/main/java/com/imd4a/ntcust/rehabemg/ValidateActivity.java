package com.imd4a.ntcust.rehabemg;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.imd4a.ntcust.rehabemg.Model.HttpResultEMGData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ValidateActivity extends AppCompatActivity {

    private WebApi webApi = new WebApi();
    private String RegisterValidateUrl = webApi.GetRegisterValidateApi();

    private Toolbar toolbar;
    private ProgressDialog progressDialog;

    private EditText EdtRegisterAccount, EdtValidateCode;
    private Button BtnValidate;

    private String register_account, validate_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);

        initView();
        setListener();

        // 加入 ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 加入 ActionBar 返回鍵
        ActionBar act = getSupportActionBar();
        act.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        EdtRegisterAccount = (EditText) findViewById(R.id.edtRegisterAccount);
        EdtValidateCode = (EditText) findViewById(R.id.edtValidateCode);

        BtnValidate = (Button) findViewById(R.id.btnValidate);
    }

    private void setListener() {
        BtnValidate.setOnClickListener(BtnValidateOnClickListener);
    }

    private View.OnClickListener BtnValidateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // 驗證
            register_account = EdtRegisterAccount.getText().toString();
            validate_code = EdtValidateCode.getText().toString();

            if (register_account.equals("") || validate_code.equals("")) {
                new AlertDialog.Builder(ValidateActivity.this)
                        .setTitle("訊息")
                        .setMessage("資料請勿空白！")
                        .setCancelable(false)
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            } else {
                showDialog("驗證中");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RegisterValidate();
                    }
                }).start();
            }
        }
    };

    //  個人分析API
    public void RegisterValidate() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", register_account);
            jsonObject.put("authcode", validate_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(RegisterValidateUrl)
                .post(body)
                .build();
        try {
            final Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissDialog();

                        new AlertDialog.Builder(ValidateActivity.this)
                                .setTitle("訊息")
                                .setMessage("驗證成功！現在可以登入！")
                                .setCancelable(false)
                                .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissDialog();

                        new AlertDialog.Builder(ValidateActivity.this)
                                .setTitle("訊息")
                                .setMessage("驗證失敗！請檢察註冊帳號與驗證碼輸入是否有錯！")
                                .setCancelable(false)
                                .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }
                });
            }
        } catch (IOException e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissDialog();

                    new AlertDialog.Builder(ValidateActivity.this)
                            .setTitle("訊息")
                            .setMessage("連線逾時！請確認網路狀態後再次驗證！")
                            .setCancelable(false)
                            .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            });

            e.printStackTrace();
        }
    }

    private void showDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        if (!progressDialog.isShowing()) {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(message);
            progressDialog.show();
        }

    }

    private void dismissDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null) return;
                progressDialog.dismiss();
                progressDialog = null;
            }
        });
    }
}
