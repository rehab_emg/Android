package com.imd4a.ntcust.rehabemg.Fragment;


import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.imd4a.ntcust.rehabemg.AnalysisActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.R;
import com.imd4a.ntcust.rehabemg.test.TestChartActivity;
import com.junkchen.blelib.BleService;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView_user;

    private LinearLayout LinearLayout_user, LinearLayout_detect, LinearLayout_history, LinearLayout_description;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        imageView_user = (ImageView) getActivity().findViewById(R.id.imageView_user);

        LinearLayout_user = (LinearLayout) getActivity().findViewById(R.id.linearlayout_user);
        LinearLayout_detect = (LinearLayout) getActivity().findViewById(R.id.linearlayout_detect);
        LinearLayout_history = (LinearLayout) getActivity().findViewById(R.id.linearlayout_history);
        LinearLayout_description = (LinearLayout) getActivity().findViewById(R.id.linearlayout_description);

        LinearLayout_user.setOnClickListener(this);
        LinearLayout_detect.setOnClickListener(this);
        LinearLayout_history.setOnClickListener(this);
        LinearLayout_description.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearlayout_user:
                ((MainActivity) getActivity()).FragmentSwitch_user();
                Log.e("user", "USER");
                break;
            case R.id.linearlayout_detect:
                Log.e("detect", "DETECT");
                ((MainActivity) getActivity()).FragmentSwitch_detect();
                break;
            case R.id.linearlayout_history:
                Log.e("history", "HISTORY");
                ((MainActivity) getActivity()).FragmentSwitch_history();
                break;
            case R.id.linearlayout_description:
                Log.e("description", "DESCRIPTION");
                ((MainActivity) getActivity()).FragmentSwitch_descripyion();
                break;
        }
    }

}
