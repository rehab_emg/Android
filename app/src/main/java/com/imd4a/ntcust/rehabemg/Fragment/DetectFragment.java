package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.imd4a.ntcust.rehabemg.DetectEMGActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.R;
import com.junkchen.blelib.MultipleBleService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetectFragment extends Fragment implements View.OnClickListener {

    // HMSoft 84:EB:18:77:B2:2E 部位一 編號：1
    // HMSoft 84:EB:18:77:92:8E 部位二 編號：2

    private Button BtnEnter;
    private Spinner SpnFoot, SpnPart;
    private TextView DetectMuscle, DetectAction, DetectPosition;
    private ImageView imageViewPosition;
    private GifImageView gifImageView;

    private String DetectFoot = "左腳";
    private String DetectPart = "大腿";
    private int foot = 0, part = 0;

    public DetectFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detect, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SpnFoot = (Spinner) getView().findViewById(R.id.spinnerFoot);
        SpnPart = (Spinner) getView().findViewById(R.id.spinnerPart);

        SpnFoot.setOnItemSelectedListener(SpnFootOnItemSelected);
        SpnPart.setOnItemSelectedListener(SpnPartOnItemSelected);

        DetectMuscle = (TextView) getView().findViewById(R.id.textViewMuscle);
        DetectMuscle.setText(R.string.thigh_muscle);
        DetectAction = (TextView) getView().findViewById(R.id.textViewAction);
        DetectAction.setText(R.string.thigh_action);
        DetectPosition = (TextView) getView().findViewById(R.id.textViewPosition);
        DetectPosition.setText("左腳大腿內、外側");

        BtnEnter = (Button) getView().findViewById(R.id.btnEnter);
        BtnEnter.setOnClickListener(this);

        imageViewPosition = (ImageView) getView().findViewById(R.id.imageViewPosition);
        gifImageView = (GifImageView) getView().findViewById(R.id.imageViewAction);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("肌電檢測");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.e(this.getClass().getName(), "再見");
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.btnEnter:
                bundle.putString("FOOT", DetectFoot);
                bundle.putString("PART", DetectPart);

                Log.e("FOOT", DetectFoot);
                Log.e("PART", DetectPart);

                Calendar date = Calendar.getInstance();
                long EMGDataTime = date.getTimeInMillis();
                Log.e("MSMSMSMSMSMSMSMSMSMSMS", String.valueOf(EMGDataTime));

                Intent it = new Intent(((MainActivity) getActivity()), DetectEMGActivity.class);
                it.putExtras(bundle);
                startActivity(it);
                break;
        }
    }

    private AdapterView.OnItemSelectedListener SpnFootOnItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            DetectFoot = parent.getSelectedItem().toString();
            foot = position;
            if (foot == 0) {
                if(part == 0){
                    imageViewPosition.setImageResource(R.drawable.left_thigh);
                    DetectPosition.setText("左腳大腿內、外側");
                }else{
                    imageViewPosition.setImageResource(R.drawable.left_calf);
                    DetectPosition.setText("左腳小腿內、外側");
                }
            } else {
                if(part == 0){
                    imageViewPosition.setImageResource(R.drawable.right_thigh);
                    DetectPosition.setText("右腳大腿內、外側");
                }else{
                    imageViewPosition.setImageResource(R.drawable.right_calf);
                    DetectPosition.setText("右腳小腿內、外側");
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener SpnPartOnItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            DetectPart = parent.getSelectedItem().toString();
            Log.e("部位", "第" + String.valueOf(position));
            if (position == 0) {
                DetectMuscle.setText(R.string.thigh_muscle);
                DetectAction.setText(R.string.thigh_action);
                gifImageView.setImageResource(R.mipmap.thigh);
            } else {
                DetectMuscle.setText(R.string.calf_muscle);
                DetectAction.setText(R.string.calf_action);
                gifImageView.setImageResource(R.mipmap.calf);
            }
            part = position;
            if (foot == 0) {
                if(part == 0){
                    imageViewPosition.setImageResource(R.drawable.left_thigh);
                    DetectPosition.setText("左腳大腿內、外側");
                }else{
                    imageViewPosition.setImageResource(R.drawable.left_calf);
                    DetectPosition.setText("左腳小腿內、外側");
                }
            } else {
                if(part == 0){
                    imageViewPosition.setImageResource(R.drawable.right_thigh);
                    DetectPosition.setText("右腳大腿內、外側");
                }else{
                    imageViewPosition.setImageResource(R.drawable.right_calf);
                    DetectPosition.setText("右腳小腿內、外側");
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
