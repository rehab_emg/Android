package com.imd4a.ntcust.rehabemg.Fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.imd4a.ntcust.rehabemg.AnalysisActivity;
import com.imd4a.ntcust.rehabemg.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Analysis2SemgFragment extends Fragment {

    // view
    private TextView semg_part1, semg_part2, isLRandPlace_View_semg, date_View_semg;

    // 圖表
    private LineChart semg_chart1, semg_chart2;
    private LineDataSet lineDataSet1, lineDataSet2;
    private LineData lineData1, lineData2;

    private XAxis xAxis1, xAxis2;
    private YAxis yAxisLeft1, yAxisLeft2;
    private YAxis yAxisRight1, yAxisRight2;

    private int LineChartBackgroundColor = Color.rgb(23, 46, 66);

    public Analysis2SemgFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_analysis2_semg, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // view
        initView();

        setChart();
        setAxis();
        Bundle b = ((AnalysisActivity) getActivity()).get_2();
        if (b != null) {
            semg_part1.setText(b.getString("part1"));
            semg_part2.setText(b.getString("part2"));
            isLRandPlace_View_semg.setText(b.getString("isLR_place"));
            date_View_semg.setText(b.getString("date"));

            setLineData(b.getString("part1"), b.getString("part2"));
            addData(b.getIntegerArrayList("semg1"), b.getIntegerArrayList("semg2"));
        }
    }

    private void initView() {
        semg_part1 = (TextView) getActivity().findViewById(R.id.semg_part1);
        semg_part2 = (TextView) getActivity().findViewById(R.id.semg_part2);
        isLRandPlace_View_semg = (TextView) getActivity().findViewById(R.id.isLRandPlace_View_semg);
        date_View_semg = (TextView) getActivity().findViewById(R.id.date_View_semg);
    }

    public void setView(String isLR, String place, String date, String part1, ArrayList<Integer> semg1, String part2, ArrayList<Integer> semg2) {
        isLRandPlace_View_semg.setText(isLR + " " + place);
        date_View_semg.setText(date);
        semg_part1.setText(part1);
        semg_part2.setText(part2);

        setChart();
        setAxis();
        setLineData(part1, part2);
        addData(semg1, semg2);
    }

    public void setChart() {
        semg_chart1 = (LineChart) getActivity().findViewById(R.id.semg_chart1);

        semg_chart1.setDrawBorders(true); // 圖表邊界
        semg_chart1.setDrawGridBackground(true); // 圖表背景顏色，如果為false，setGridBackgroundColor失效
        semg_chart1.setGridBackgroundColor(LineChartBackgroundColor); // 背景顏色
        semg_chart1.getDescription().setEnabled(false); // 描述文字

        semg_chart1.setTouchEnabled(true); // 啟動圖表手勢（縮放、拖動等等）
        semg_chart1.setDragEnabled(true); // 圖表拖動
        semg_chart1.setScaleEnabled(true); // 圖表縮放
        semg_chart1.setPinchZoom(false); // 如果為false，則可分別在X軸或Y軸上進行縮放

        semg_chart1.invalidate();

        semg_chart2 = (LineChart) getActivity().findViewById(R.id.semg_chart2);

        semg_chart2.setDrawBorders(true); // 圖表邊界
        semg_chart2.setDrawGridBackground(true); // 圖表背景顏色，如果為false，setGridBackgroundColor失效
        semg_chart2.setGridBackgroundColor(LineChartBackgroundColor); // 背景顏色
        semg_chart2.getDescription().setEnabled(false); // 描述文字

        semg_chart2.setTouchEnabled(true); // 啟動圖表手勢（縮放、拖動等等）
        semg_chart2.setDragEnabled(true); // 圖表拖動
        semg_chart2.setScaleEnabled(true); // 圖表縮放
        semg_chart2.setPinchZoom(false); // 如果為false，則可分別在X軸或Y軸上進行縮放

        semg_chart2.invalidate();
    }

    private void setAxis() {
        // X軸
        xAxis1 = semg_chart1.getXAxis(); // 取得圖表之X軸
        xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM); // 設定位置為圖表下方（預設上方）
        xAxis1.setAvoidFirstLastClipping(true); // 避免第一與最後的項目被圖表裁減掉
        xAxis1.setAxisMinimum(0f); // 起始值
//        xAxis1.setAxisMaximum(5400f);
        xAxis1.setEnabled(true);

        xAxis2 = semg_chart2.getXAxis(); // 取得圖表之X軸
        xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM); // 設定位置為圖表下方（預設上方）
        xAxis2.setAvoidFirstLastClipping(true); // 避免第一與最後的項目被圖表裁減掉
        xAxis2.setAxisMinimum(0f); // 起始值
//        xAxis2.setAxisMaximum(5400f);
        xAxis2.setEnabled(true);

        // Y軸
        yAxisLeft1 = semg_chart1.getAxisLeft(); // 取得圖表之Y軸（左方）
        yAxisLeft1.setInverted(false); // Y軸方向設定（如果為true，則最小值在上方，最大值在下方）
        yAxisLeft1.setAxisMinimum(0f); // 起始值
//        yAxisLeft1.setAxisMaximum(1000f); // 最大值
        yAxisLeft1.setEnabled(true);

        yAxisRight1 = semg_chart1.getAxisRight(); // 取得圖表之Y軸（右方）
        yAxisRight1.setEnabled(false); // 是否啟用

        yAxisLeft2 = semg_chart2.getAxisLeft(); // 取得圖表之Y軸（左方）
        yAxisLeft2.setInverted(false); // Y軸方向設定（如果為true，則最小值在上方，最大值在下方）
        yAxisLeft2.setAxisMinimum(0f); // 起始值
//        yAxisLeft2.setAxisMaximum(1000f); // 最大值
        yAxisLeft2.setEnabled(true);

        yAxisRight2 = semg_chart2.getAxisRight(); // 取得圖表之Y軸（右方）
        yAxisRight2.setEnabled(false); // 是否啟用
    }

    private void setLineData(String part1, String part2) {
        lineDataSet1 = new LineDataSet(null, part1);
        lineDataSet1.setLineWidth(1.5f); // 折線圖線條寬度
        lineDataSet1.setDrawCircles(false); // 折線圖圓點啟用
        lineDataSet1.setColor(Color.WHITE);
        lineDataSet1.setHighLightColor(Color.GREEN);

        lineDataSet2 = new LineDataSet(null, part2);
        lineDataSet2.setLineWidth(1.5f); // 折線圖線條寬度
        lineDataSet2.setDrawCircles(false); // 折線圖圓點啟用
        lineDataSet2.setColor(Color.YELLOW);
        lineDataSet2.setHighLightColor(Color.GREEN);

        lineData1 = new LineData();
        semg_chart1.setData(lineData1);
        semg_chart1.invalidate();

        lineData2 = new LineData();
        semg_chart2.setData(lineData2);
        semg_chart2.invalidate();
    }

    private float max_1 = 0, min_1 = 0;
    private float max_2 = 0, min_2 = 0;

    // 資料
    private void addData(ArrayList<Integer> data1, ArrayList<Integer> data2) {
        xAxis1.setAxisMaximum(data1.size());
        xAxis2.setAxisMaximum(data2.size());

        if (lineDataSet1.getEntryCount() == 0) {
            lineData1.addDataSet(lineDataSet1);
            lineData2.addDataSet(lineDataSet2);
        }
        semg_chart1.setData(lineData1);
        semg_chart2.setData(lineData2);

        for (int i = 0; i < data1.size(); i++) {
            float xVal = lineDataSet1.getEntryCount();
            float yVal = (float) data1.get(i);
            Entry entry = new Entry(xVal, yVal);

            // 圖表1
            if (i <= (data1.size() - 1)) {
                lineData1.getDataSetByIndex(0).addEntry(entry);

                if (yVal > max_1) {
                    max_1 = yVal + 50;
                    yAxisLeft1.setAxisMaximum(max_1);
                } else if (yVal < min_1) {
                    if (yVal - 50 > 0) {
                        min_1 = yVal - 50;
                    } else {
                        min_1 = 0;
                    }
                    yAxisLeft1.setAxisMinimum(min_1);
                }

                lineData1.notifyDataChanged();
            }

            // 圖表2
            if (i <= (data2.size() - 1)) {
                xVal = lineDataSet2.getEntryCount();
                yVal = (float) data2.get(i);

                entry = new Entry(xVal, yVal);
                lineData2.getDataSetByIndex(0).addEntry(entry);

                if (yVal > max_2) {
                    max_2 = yVal + 50;
                    yAxisLeft2.setAxisMaximum(max_2);
                } else if (yVal < min_2) {
                    if (yVal - 50 > 0) {
                        min_2 = yVal - 50;
                    } else {
                        min_2 = 0;
                    }
                    yAxisLeft2.setAxisMinimum(min_2);
                }

                lineData2.notifyDataChanged();
            }


            if (i == 0) {
                max_1 = data1.get(i);
                max_2 = data2.get(i);
                min_1 = data1.get(i);
                min_2 = data2.get(i);
            }
        }
        semg_chart1.notifyDataSetChanged();
        semg_chart2.notifyDataSetChanged();

        semg_chart1.setData(lineData1); // 將折線圖資料放入圖表
        semg_chart1.invalidate(); // 刷新（更新）圖表

        semg_chart2.setData(lineData2); // 將折線圖資料放入圖表
        semg_chart2.invalidate(); // 刷新（更新）圖表
    }
}
