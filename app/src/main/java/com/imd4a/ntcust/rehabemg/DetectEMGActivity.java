package com.imd4a.ntcust.rehabemg;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.anastr.speedviewlib.Speedometer;
import com.github.anastr.speedviewlib.TubeSpeedometer;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.junkchen.blelib.BleService;
import com.junkchen.blelib.MultipleBleService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DetectEMGActivity extends AppCompatActivity implements View.OnClickListener {

    private WebApi webApi = new WebApi();

    private String Account;
    private String Url_Before = webApi.GetEmg_PreApi();
    private String Url_detect = webApi.GetEmgApi();
    private String Url_after = webApi.GetEmg_PostApi();

    // region BLE
    // 只有 Characteristic UUID 能與 Service UUID 進行通信，兩者相對應
    private static String GATT_SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb"; // BLE Service UUID
    private static String CHARACTERISTIC_TX = "0000ffe1-0000-1000-8000-00805f9b34fb"; // BLE Characteristic UUID 發送數據
    private static String CHARACTERISTIC_RX = "0000ffe1-0000-1000-8000-00805f9b34fb"; // BLE Characteristic UUID 接收數據

    private static final int SCAN_TIME = 5000; // Bluetooth掃描時間
    private static final String BLE_DEVICE_NAME = "HMSoft"; // 連接裝置名稱

    private static final String BLE_ADDRESS_PLACE1 = "84:EB:18:77:B2:2E"; // 部位一
    private static final String BLE_ADDRESS_PLACE2 = "84:EB:18:77:92:8E"; // 部位二

    private MultipleBleService mBleService;
    private boolean mIsBind = false; // mBleService 是否綁定
    private boolean mIsConnect = false; // 是否連接
    private boolean isReadData = false; // 是否在檢測資料
    private boolean device_1_isConnect = false;
    private boolean device_2_isConnect = false;
    private boolean device_1_isReady = false;
    private boolean device_2_isReady = false;
    private ArrayList<String> bleAddresses = new ArrayList<String>(); // 欲連接之設備位址

    public static final int REQUEST_CODE_ACCESS_COARSE_LOCATION = 1;
    //endregion

    //region EMG
    private int EMGdata = 0; // 收集到的肌電

    private ArrayList<Integer> EMG_Data_List_Before_1 = new ArrayList<>();
    private ArrayList<Integer> EMG_Data_List_Before_2 = new ArrayList<>();
    private ArrayList<Integer> EMG_Data_List_detect_1 = new ArrayList<>();
    private ArrayList<Integer> EMG_Data_List_detect_2 = new ArrayList<>();
    private ArrayList<Integer> EMG_Data_List_After_1 = new ArrayList<>();
    private ArrayList<Integer> EMG_Data_List_After_2 = new ArrayList<>();
    private long StartTime;
    private JSONArray obj_array_Before_1 = new JSONArray();
    private JSONArray obj_array_Before_2 = new JSONArray();
    private JSONArray obj_array_Detect_1 = new JSONArray();
    private JSONArray obj_array_Detect_2 = new JSONArray();
    private JSONArray obj_array_After_1 = new JSONArray();
    private JSONArray obj_array_After_2 = new JSONArray();

    private String DetectFoot;
    private String DetectPart;

    private int detect_status = 1; // 1：檢測前，2：檢測中，3：檢測後
    //endregion

    //region 圖表
    private LineChart lineChart1;
    private LineDataSet lineDataSet1;
    private LineData lineData1;
    private XAxis xAxis1; // 圖表X軸
    private YAxis yLeftAxis1; // 圖表Y軸（左方）
    private YAxis yRightAxis1; // 圖表Y軸（右方）

    private LineChart lineChart2; // 折線圖表
    private LineDataSet lineDataSet2; // 折線圖資料設定
    private LineData lineData2; // 折線圖資料物件
    private XAxis xAxis2; // 圖表X軸
    private YAxis yLeftAxis2; // 圖表Y軸（左方）
    private YAxis yRightAxis2; // 圖表Y軸（右方）

    private int LineChartBackgroundColor = Color.rgb(23, 46, 66);
    //endregion

    // 聲音檔設定
    private static final int SOUND_COUNT = 1;
    private SoundPool soundPool;
    private int detect_before_id, detect_after_id, detect_action_id, detect_start_id, detect_end_id, oneTothree_id;

    private ProgressDialog progressDialog;

    private Handler handler = new Handler();

    private Button BtnStart, BtnStop, BtnConnect, BtnUpload;
    private TextView TextTitle1, TextTitle2, TextTitle3, TextPart1, TextPart2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detect_emg);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 保持喚醒

        Intent it = getIntent();
        Bundle bundle = it.getExtras();

        DetectFoot = bundle.getString("FOOT");
        DetectPart = bundle.getString("PART");

        InitView();
        SetListener();
        InitVariable();
        InitChart();
        setSound(); // 載入聲音檔

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        callAltDlg("有語音功能，請先調整適當音量");

        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        Account = sharedPreferences.getString("Account", "未登入");
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            confirmExit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void confirmExit() {
        final android.app.AlertDialog.Builder alrDlg = new android.app.AlertDialog.Builder(DetectEMGActivity.this);
        alrDlg.setTitle("訊息");
        alrDlg.setMessage("離開前請確定檢測資料已上傳\r\n確定要離開嗎？");
        alrDlg.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                DetectEMGActivity.this.onDestroy();
                DetectEMGActivity.this.finish();
            }
        });
        alrDlg.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alrDlg.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("888888", "888888888888888888");
        doUnBindService();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                bleServiceStartRead();
                break;
            case R.id.btnStop:
                if (isReadData) {
                    AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(DetectEMGActivity.this)
                            .setTitle("訊息")
                            .setMessage("檢測尚未結束，確定要提早停止嗎？")
                            .setCancelable(false)
                            .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                    altDlgBuilder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            bleServiceStopRead();
                        }
                    });
                    altDlgBuilder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    altDlgBuilder.show();
                } else {
                    bleServiceStopRead();
                }
                break;
            case R.id.btnConnect:
                bleServiceReset();
                break;
            case R.id.btnUpload:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        detectemgpost_pre();
                    }
                }).start();
                break;
        }
    }

    private void InitView() {
        BtnStart = findViewById(R.id.btnStart);
        BtnStop = findViewById(R.id.btnStop);
        BtnConnect = findViewById(R.id.btnConnect);
        BtnUpload = findViewById(R.id.btnUpload);

        TextTitle1 = findViewById(R.id.textViewTitle1);
        TextTitle2 = findViewById(R.id.textViewTitle2);
        TextTitle3 = findViewById(R.id.textViewTitle3);

        TextPart1 = findViewById(R.id.textViewPart1);
        TextPart2 = findViewById(R.id.textViewPart2);
    }

    private void SetListener() {
        BtnStart.setOnClickListener(this);
        BtnStop.setOnClickListener(this);
        BtnConnect.setOnClickListener(this);
        BtnUpload.setOnClickListener(this);
    }

    private void InitVariable() {
        EMG_Data_List_Before_1.clear();
        EMG_Data_List_Before_2.clear();

        EMG_Data_List_detect_1.clear();
        EMG_Data_List_detect_2.clear();

        EMG_Data_List_After_1.clear();
        EMG_Data_List_After_2.clear();
    }

    private void InitChart() {
        lineChart1 = (LineChart) findViewById(R.id.chart1);
        lineChart2 = (LineChart) findViewById(R.id.chart2);

        setChart(); // 折線圖表設定
        setAxis(); // X、Y軸設定
        setLineData();
    }

    private void callAltDlg(String msg) {
        AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(DetectEMGActivity.this)
                .setTitle("訊息")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        altDlgBuilder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void setChart() {
        lineChart1.setDrawBorders(true); // 圖表邊界
        lineChart1.setDrawGridBackground(true); // 圖表背景顏色，如果為false，setGridBackgroundColor失效
        lineChart1.setGridBackgroundColor(LineChartBackgroundColor); // 背景顏色
        lineChart1.getDescription().setEnabled(false); // 描述文字

        lineChart1.setTouchEnabled(true); // 啟動圖表手勢（縮放、拖動等等）
        lineChart1.setDragEnabled(true); // 圖表拖動
        lineChart1.setScaleEnabled(true); // 圖表縮放
        lineChart1.setPinchZoom(false); // 如果為false，則可分別在X軸或Y軸上進行縮放

        lineChart1.invalidate();

        lineChart2.setDrawBorders(true); // 圖表邊界
        lineChart2.setDrawGridBackground(true); // 圖表背景顏色，如果為false，setGridBackgroundColor失效
        lineChart2.setGridBackgroundColor(LineChartBackgroundColor); // 背景顏色
        lineChart2.getDescription().setEnabled(false); // 描述文字

        lineChart2.setTouchEnabled(true); // 啟動圖表手勢（縮放、拖動等等）
        lineChart2.setDragEnabled(true); // 圖表拖動
        lineChart2.setScaleEnabled(true); // 圖表縮放
        lineChart2.setPinchZoom(false); // 如果為false，則可分別在X軸或Y軸上進行縮放

//        lineChart.setOnChartValueSelectedListener(lineChartOnChartValueSelected); // 圖表數值Selected監聽事件
//        lineChart.setBackgroundColor(Color.RED); // 替代背景顏色
        lineChart2.invalidate(); // 刷新（更新）圖表
    }

    private void setAxis() {
        // X軸
        xAxis1 = lineChart1.getXAxis(); // 取得圖表之X軸
        xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM); // 設定位置為圖表下方（預設上方）
        xAxis1.setAvoidFirstLastClipping(true); // 避免第一與最後的項目被圖表裁減掉
        xAxis1.setAxisMinimum(0f); // 起始值
        xAxis1.setAxisMaximum(100f);
        xAxis1.setEnabled(false);

        xAxis2 = lineChart2.getXAxis(); // 取得圖表之X軸
        xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM); // 設定位置為圖表下方（預設上方）
        xAxis2.setAvoidFirstLastClipping(true); // 避免第一與最後的項目被圖表裁減掉
        xAxis2.setAxisMinimum(0f); // 起始值
        xAxis2.setAxisMaximum(100f);
        xAxis2.setEnabled(false);

        // Y軸
        yLeftAxis1 = lineChart1.getAxisLeft(); // 取得圖表之Y軸（左方）
        yLeftAxis1.setInverted(false); // Y軸方向設定（如果為true，則最小值在上方，最大值在下方）
        yLeftAxis1.setAxisMinimum(250f); // 起始值
        yLeftAxis1.setAxisMaximum(350f); // 最大值
        yLeftAxis1.setEnabled(true);

        yRightAxis1 = lineChart1.getAxisRight(); // 取得圖表之Y軸（右方）
        yRightAxis1.setEnabled(false); // 是否啟用

        yLeftAxis2 = lineChart2.getAxisLeft(); // 取得圖表之Y軸（左方）
        yLeftAxis2.setInverted(false); // Y軸方向設定（如果為true，則最小值在上方，最大值在下方）
        yLeftAxis2.setAxisMinimum(250f); // 起始值
        yLeftAxis2.setAxisMaximum(350f); // 最大值
        yLeftAxis2.setEnabled(true);

        yRightAxis2 = lineChart2.getAxisRight(); // 取得圖表之Y軸（右方）
        yRightAxis2.setEnabled(false); // 是否啟用
    }

    private void setLineData() {
        String muscle_1, muscle_2;
        if (DetectPart.equals("大腿")) {
            muscle_1 = "股內側肌";
            muscle_2 = "股外側肌";
        } else {
            muscle_1 = "腓腸肌(內)";
            muscle_2 = "腓腸肌(外)";
        }
        TextPart1.setText("部位一：" + muscle_1);
        TextPart2.setText("部位二：" + muscle_2);

        lineDataSet1 = new LineDataSet(null, muscle_1);
        lineDataSet1.setLineWidth(1.5f); // 折線圖線條寬度
        lineDataSet1.setDrawCircles(false); // 折線圖圓點啟用
        lineDataSet1.setColor(Color.WHITE);
        lineDataSet1.setHighLightColor(Color.GREEN);

        lineData1 = new LineData();
        lineChart1.setData(lineData1);
        lineChart1.invalidate();

        lineDataSet2 = new LineDataSet(null, muscle_2);
        lineDataSet2.setLineWidth(1.5f); // 折線圖線條寬度
        lineDataSet2.setDrawCircles(false); // 折線圖圓點啟用
        lineDataSet2.setColor(Color.WHITE);
        lineDataSet2.setHighLightColor(Color.GREEN);

        lineData2 = new LineData();
        lineChart2.setData(lineData2);
        lineChart2.invalidate();

//        lineDataSet.setCircleRadius(4f); // 折線圖資料圓點半徑
//        lineDataSet.setCircleColor(Color.RED);
    }

    private void setSound() {
        soundPool = new SoundPool(SOUND_COUNT, AudioManager.STREAM_MUSIC, 0);
        detect_start_id = soundPool.load(this, R.raw.detect_start, 1);
        detect_end_id = soundPool.load(this, R.raw.detect_end, 1);
        oneTothree_id = soundPool.load(this, R.raw.one_two_three, 1);
        detect_before_id = soundPool.load(this, R.raw.detect_before, 1);
        detect_after_id = soundPool.load(this, R.raw.detect_after, 1);
        detect_action_id = soundPool.load(this, R.raw.detect_action, 1);
    }

    private void detectStart() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                soundPool.play(detect_start_id, 1, 1, 0, 0, 1);
            }
        }, 2000);
        if (detect_status == 1) {
            soundPool.play(detect_before_id, 1, 1, 0, 0, 1);
        } else if (detect_status == 2) {
            soundPool.play(detect_action_id, 1, 1, 0, 0, 1);
        } else if (detect_status == 3) {
            soundPool.play(detect_after_id, 1, 1, 0, 0, 1);
        }
    }

    private void detectEnd() {
        soundPool.play(detect_end_id, 1, 1, 0, 0, 1);
    }

    private void oneTothree() {
        soundPool.play(oneTothree_id, 1, 1, 0, 0, 1);
    }

    private float minData1 = 250;
    private float maxData1 = 350;
    private float minData2 = 250;
    private float maxData2 = 350;

    private void addData(final int data, final int place) {
        if (mIsConnect) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (place == 1) {
                        if (lineDataSet1.getEntryCount() == 0) {
                            lineData1.addDataSet(lineDataSet1);
                        }
                        lineChart1.setData(lineData1);

                        float xVal = lineDataSet1.getEntryCount();
                        float yVal = ((float) data);

                        if (yVal < minData1) {
                            minData1 = (yVal - 50 < 0) ? 0 : yVal - 50;
                            yLeftAxis1.setAxisMinimum(minData1);
                        } else if (yVal > maxData1) {
                            maxData1 = (yVal + 50 > 1000) ? 1000 : yVal + 50;
                            yLeftAxis1.setAxisMaximum(maxData1);
                        }

                        Entry entry = new Entry(xVal, yVal);
                        Log.e("X數值", String.valueOf(xVal));
                        lineData1.addEntry(entry, 0);

                        lineData1.notifyDataChanged();
                        lineChart1.notifyDataSetChanged();

                        lineChart1.setData(lineData1); // 將折線圖資料放入圖表

                        if (xVal < 100) {
                            xAxis1.setAxisMinimum(0);
                            xAxis1.setAxisMaximum(100);
                        } else {
                            xAxis1.setAxisMinimum(xVal - 99);
                            xAxis1.setAxisMaximum(xVal);
                        }

                        lineChart1.invalidate(); // 刷新（更新）圖表
                    } else {
                        if (lineDataSet2.getEntryCount() == 0) {
                            lineData2.addDataSet(lineDataSet2);
                        }
                        lineChart2.setData(lineData2);

                        float xVal = lineDataSet2.getEntryCount();
                        float yVal = ((float) data);

                        if (yVal < minData2) {
                            minData2 = (yVal - 50 < 0) ? 0 : yVal - 50;
                            yLeftAxis2.setAxisMinimum(minData2);
                        } else if (yVal > maxData2) {
                            maxData2 = (yVal + 50 > 1000) ? 1000 : yVal + 50;
                            yLeftAxis2.setAxisMaximum(maxData2);
                        }

                        Entry entry = new Entry(xVal, yVal);
                        Log.e("X數值", String.valueOf(xVal));
                        lineData2.addEntry(entry, 0);

                        lineData2.notifyDataChanged();
                        lineChart2.notifyDataSetChanged();

                        lineChart2.setData(lineData2); // 將折線圖資料放入圖表

                        if (xVal < 100) {
                            xAxis2.setAxisMinimum(0);
                            xAxis2.setAxisMaximum(100);
                        } else {
                            xAxis2.setAxisMinimum(xVal - 99);
                            xAxis2.setAxisMaximum(xVal);
                        }

                        lineChart2.invalidate(); // 刷新（更新）圖表
                    }

                }
            });
        }
    }

    //region BLE

    public void ToastMsg(String msg) {
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void showDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        if (!progressDialog.isShowing()) {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(message);
            progressDialog.show();
        }

    }

    private void dismissDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null) return;
                progressDialog.dismiss();
                progressDialog = null;
            }
        });
    }

    private ServiceConnection serviceConnection = new ServiceConnection() { // BLE Service 綁定回調
        @Override // 連接Service
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBleService = ((MultipleBleService.LocalBinder) service).getService();
            if (bleServiceInitialize()) {
                if (bleServiceEnableBlt()) {
                    mIsBind = true;
                    setBleServiceListener();
//                    ifRequestPermission(); // 判斷系統是否需要動態獲取權限（Android 6.0以上需使用）
//                    mBleService.scanLeDevice(true);
//                    bleServiceScanLeDevice();
//                    Log.i(this.getClass().getName(), "藍芽已開啟");

                    CheckPermission();
                }
            } else {
                Log.e(this.getClass().getName(), "不支援藍芽");
            }
        }

        @Override // 斷開Service連接
        public void onServiceDisconnected(ComponentName name) {
            Log.e(this.getClass().getName(), "Service連接中斷");
            mBleService = null;
            mIsBind = false;
        }
    };

    public void doBindService() { // 綁定Service
        Log.i(this.getClass().getName(), "Service");
        Intent serviceIntent = new Intent(this, MultipleBleService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void doUnBindService() { // 解除Service綁定
        if (mIsBind) {
            Log.e(this.getClass().getName(), "解除Service綁定");
            unbindService(serviceConnection);
            mBleService = null;
            mIsBind = false;
            mIsConnect = false;
            isReadData = false;
            device_1_isConnect = false;
            device_2_isConnect = false;
        }
    }

    private void CheckPermission() {
        // API是否大於23，Android 6.0以上
        if (Build.VERSION.SDK_INT >= 23) {
            // 程式是否有獲得權限
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // 說明為何要申請權限
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    ToastMsg("Android 6.0版本以上須獲得權限程式才能正確運行");
                }
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_ACCESS_COARSE_LOCATION);
            } else if (isLocationEnable(this)) {
                // 掃描BLE
                Log.e("掃描藍芽", "BLE BLE BLE");
                bleServiceScanLeDevice();
            } else {
                Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                this.startActivityForResult(locationIntent, 2);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ACCESS_COARSE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 允許給予權限，0表示允許，-1表示拒絕，PERMISSION = 0, PERMISSION_DENIED = -1
                if (isLocationEnable(this)) {
                    // 掃描BLE
                    Log.e("掃描藍芽", "BLE BLE BLE");
                    bleServiceScanLeDevice();
                } else {
                    Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    this.startActivityForResult(locationIntent, 2);
                }
            } else {
                // 拒絕給予權限

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean isLocationEnable(Context context) { // 檢查是否開啟定位
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean networkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsProvider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (networkProvider || gpsProvider) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if (isLocationEnable(this)) {
                // 定位已開啟
                // 掃描BLE
                Log.e("掃描藍芽", "BLE BLE BLE");
                bleServiceScanLeDevice();

            } else {
                // 定位未開啟
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public boolean bleServiceInitialize() { // BLE 初始化
        Log.i(this.getClass().getName(), "BLE 初始化");
        return mBleService.initialize(); // 初始化
    }

    public boolean bleServiceEnableBlt() { // 開啟藍芽
        Log.i(this.getClass().getName(), "開啟藍芽");
        boolean enable = true;
        return mBleService.enableBluetooth(enable); // 開啟或關閉
    }

    public void bleServiceScanLeDevice() { // 掃描 BLE 設備
        if (!mBleService.isScanning()) {
            Log.i(this.getClass().getName(), "掃描 BLE 設備");
            SystemClock.sleep(500);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissDialog();
                }
            }, SCAN_TIME);

            showDialog("掃描中");
            boolean enable = true;
            mBleService.scanLeDevice(enable, SCAN_TIME); // 開始或停止掃描
        } else {
            Log.e(this.getClass().getName(), "正在掃描");
        }
    }

    public void bleServiceConnect() { // 連接 BLE 設備
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (bleAddresses.size() > 0) {
                    if (!mIsConnect) {
                        showDialog("設備連接中");
                        for (String address : bleAddresses) {
                            Log.i(this.getClass().getName(), "連接 BLE 設備：" + address);
                            mIsConnect = mBleService.connect(address);
                        }
                    } else {
                        Log.i(this.getClass().getName(), "已連接");
                    }
                } else {
                    dismissDialog();
                    callAltDlg("無可連接之設備，請開啟檢測器，並按掃描裝置按鈕");
                    Log.e(this.getClass().getName(), "無可連接之設備");
                }
            }
        }, 100);

//        mBleService.connect(bleAddress); // 連接
//        mBleService.disconnect(); // 取消連接
    }

    public void bleServiceStartRead() { // 設置通知
        if (mIsConnect) {
            if (!isReadData) {
                if (bleAddresses.size() == 2) {
                    int delaytime = 4000;

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isReadData = true;

                            Calendar time = Calendar.getInstance();
                            StartTime = time.getTimeInMillis();

                            for (String address : bleAddresses) {
                                ToastMsg("開始檢測");
                                Log.i(this.getClass().getName(), "開始檢測 " + address);
                                mBleService.setCharacteristicNotification(address, GATT_SERVICE_UUID, CHARACTERISTIC_RX, true); // 設置通知
                                mBleService.readCharacteristic(address, GATT_SERVICE_UUID, CHARACTERISTIC_RX); // 讀取數據
                            }
                        }
                    }, delaytime);
                    detectStart();
                }
            }
        } else {
            callAltDlg("請先連接設備");
            ToastMsg("尚未連接設備或連接失敗");
            Log.e(this.getClass().getName(), "請先連接設備");
        }

//        mBleService.writeCharacteristic(); // 寫入數據
    }

    public void bleServiceStopRead() {
        if (mIsConnect) {
            if (isReadData) {
                ToastMsg("檢測結束");
                Log.i(this.getClass().getName(), "停止檢測");
                isReadData = false;
                for (String address : bleAddresses) {
                    mBleService.setCharacteristicNotification(address, GATT_SERVICE_UUID, CHARACTERISTIC_RX, false); // 設置通知
                }

                if (detect_status <= 3) {
                    detect_status += 1;
                }
                if (detect_status == 2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextTitle1.setTextColor(getResources().getColor(R.color.color_gray_dark));
                            TextTitle2.setTextColor(getResources().getColor(R.color.color_white_light));
                            TextTitle3.setTextColor(getResources().getColor(R.color.color_gray_dark));
                            callAltDlg("接下來進行動做檢測");
                        }
                    });
                } else if (detect_status == 3) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextTitle1.setTextColor(getResources().getColor(R.color.color_gray_dark));
                            TextTitle2.setTextColor(getResources().getColor(R.color.color_gray_dark));
                            TextTitle3.setTextColor(getResources().getColor(R.color.color_white_light));
                            callAltDlg("最後進行動作後檢測");
                        }
                    });
                }
                detect_num_1 = 0;
                detect_num_2 = 0;

                if (obj_array_Before_1.length() > 0 && obj_array_Before_2.length() > 0 && obj_array_Detect_1.length() > 0 && obj_array_Detect_2.length() > 0 && obj_array_After_1.length() > 0 && obj_array_After_2.length() > 0) {
                    BtnStatusSwitch(false, false, false);
                    BtnUpload_switch(true);
                }

                Log.e("檢測前OBJECT", obj_array_Before_1.toString());
                Log.e("檢測前OBJECT", obj_array_Before_2.toString());
                Log.e("檢測中OBJECT", obj_array_Detect_1.toString());
                Log.e("檢測中OBJECT", obj_array_Detect_2.toString());
                Log.e("檢測後OBJECT", obj_array_After_1.toString());
                Log.e("檢測後OBJECT", obj_array_After_2.toString());

                Log.e("檢測前OBJECT陣列資料數量1", String.valueOf(obj_array_Before_1.length()));
                Log.e("檢測前OBJECT陣列資料數量2", String.valueOf(obj_array_Before_2.length()));
                Log.e("檢測中OBJECT陣列資料數量1", String.valueOf(obj_array_Detect_1.length()));
                Log.e("檢測中OBJECT陣列資料數量2", String.valueOf(obj_array_Detect_2.length()));
                Log.e("檢測後OBJECT陣列資料數量1", String.valueOf(obj_array_After_1.length()));
                Log.e("檢測後OBJECT陣列資料數量2", String.valueOf(obj_array_After_2.length()));

                Log.e("檢測前OBJECT陣列資料數量1", String.valueOf(EMG_Data_List_Before_1.size()));
                Log.e("檢測前OBJECT陣列資料數量2", String.valueOf(EMG_Data_List_Before_2.size()));
                Log.e("檢測中OBJECT陣列資料數量1", String.valueOf(EMG_Data_List_detect_1.size()));
                Log.e("檢測中OBJECT陣列資料數量2", String.valueOf(EMG_Data_List_detect_2.size()));
                Log.e("檢測後OBJECT陣列資料數量1", String.valueOf(EMG_Data_List_After_1.size()));
                Log.e("檢測後OBJECT陣列資料數量2", String.valueOf(EMG_Data_List_After_2.size()));
            }
        }
    }

    public void bleServiceReset() {
        if (mIsConnect) {
            mBleService.disconnect(BLE_ADDRESS_PLACE1);
            mBleService.disconnect(BLE_ADDRESS_PLACE2);
        }
        doUnBindService();
        mIsBind = false; // mBleService 是否綁定
        mIsConnect = false; // 是被是否連接
        device_1_isConnect = false;
        device_2_isConnect = false;
        isReadData = false; // 是否在檢測資料
        device_1_isReady = false;
        device_2_isReady = false;
        bleAddresses.clear(); // 欲連接之設備位址
        BtnStatusSwitch(true, false, false);
        doBindService();
    }

    private void BtnStatusSwitch(boolean connect, boolean start, boolean stop) {
        BtnConnect_switch(connect);
        BtnStart_switch(start);
        BtnStop_switch(stop);
    }

    private void BtnConnect_switch(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    BtnConnect.setEnabled(true);
                    BtnConnect.setTextColor(getResources().getColor(R.color.color_white_light));
                    BtnConnect.setBackground(getResources().getDrawable(R.drawable.detect_btn_on));
                } else {
                    BtnConnect.setEnabled(false);
                    BtnConnect.setTextColor(getResources().getColor(R.color.color_blue_dark));
                    BtnConnect.setBackground(getResources().getDrawable(R.drawable.detect_btn_off));
                }
            }
        });
    }

    private void BtnStart_switch(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    BtnStart.setEnabled(true);
                    BtnStart.setTextColor(getResources().getColor(R.color.color_white_light));
                    BtnStart.setBackground(getResources().getDrawable(R.drawable.detect_btn_on));
                } else {
                    BtnStart.setEnabled(false);
                    BtnStart.setTextColor(getResources().getColor(R.color.color_blue_dark));
                    BtnStart.setBackground(getResources().getDrawable(R.drawable.detect_btn_off));
                }
            }
        });
    }

    private void BtnStop_switch(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    BtnStop.setEnabled(true);
                    BtnStop.setTextColor(getResources().getColor(R.color.color_white_light));
                    BtnStop.setBackground(getResources().getDrawable(R.drawable.detect_btn_on));
                } else {
                    BtnStop.setEnabled(false);
                    BtnStop.setTextColor(getResources().getColor(R.color.color_blue_dark));
                    BtnStop.setBackground(getResources().getDrawable(R.drawable.detect_btn_off));
                }
            }
        });
    }

    private void BtnUpload_switch(final boolean b) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    BtnUpload.setEnabled(true);
                    BtnUpload.setTextColor(getResources().getColor(R.color.color_white_light));
                    BtnUpload.setBackground(getResources().getDrawable(R.drawable.detect_btn_on));
                } else {
                    BtnUpload.setEnabled(false);
                    BtnUpload.setTextColor(getResources().getColor(R.color.color_blue_dark));
                    BtnUpload.setBackground(getResources().getDrawable(R.drawable.detect_btn_off));
                }
            }
        });
    }

    private void setBleServiceListener() { // 設置 BLE Service 監聽
        mBleService.setOnLeScanListener(new BleService.OnLeScanListener() { // BLE掃描回調
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) { // 每掃描到一個 BLE 設備時回調（掃描結果重複於library處理）
                String deviceName = (device.getName() != null) ? device.getName() : "Unknown Name";
                String deviceAddress = (device.getAddress() != null) ? device.getAddress() : "Unknown Address";

                if (deviceName.equals(BLE_DEVICE_NAME)) { // 取得指定 BLE 設備
                    if (!bleAddresses.contains(deviceAddress)) {
                        bleAddresses.add(deviceAddress); // 取得指定設備之位址（連接用）
                        Log.e(this.getClass().getName(), "設備名稱：" + deviceName + "，設備位址：" + deviceAddress);
                        if (deviceAddress.equals(BLE_ADDRESS_PLACE1)) {
                            device_1_isReady = true;
                        } else if (deviceAddress.equals(BLE_ADDRESS_PLACE2)) {
                            device_2_isReady = true;
                        }
                        if (device_1_isReady && device_2_isReady) {
                            dismissDialog();
                            bleServiceConnect();
                        }
                    }
                }
            }
        });

        mBleService.setOnConnectListener(new BleService.OnConnectionStateChangeListener() { // BLE 連接回調
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) { // BLE 設備連接狀態改變回調
                String address = (gatt.getDevice().getAddress() != null) ? gatt.getDevice().getAddress() : "Unknown";

                if (newState == BluetoothProfile.STATE_DISCONNECTED) { // BluetoothProfile.STATE_DISCONNECTED = 0
                    // BLE 連接斷開
                    if (mIsConnect) {
                        mBleService.disconnect(gatt.getDevice().getAddress());
                    }

                    mIsConnect = false;
                    if (address.equals(BLE_ADDRESS_PLACE1)) {
                        device_1_isConnect = false;
                    } else if (address.equals(BLE_ADDRESS_PLACE2)) {
                        device_2_isConnect = false;
                    }

                    BtnStatusSwitch(true, false, false);

                    Log.i(this.getClass().getName(), "BLE 連接斷開：" + address);
                } else if (newState == BluetoothProfile.STATE_CONNECTING) { // BluetoothProfile.STATE_CONNECTING = 1
                    // BLE 正在連接
                    Log.i(this.getClass().getName(), "BLE 正在連接：" + address);
                } else if (newState == BluetoothProfile.STATE_CONNECTED) { // BluetoothProfile.STATE_CONNECTED = 2
                    // BLE 已連接
                    if (address.equals(BLE_ADDRESS_PLACE1)) {
                        device_1_isConnect = true;
                    } else if (address.equals(BLE_ADDRESS_PLACE2)) {
                        device_2_isConnect = true;
                    }

                    if (device_1_isConnect && device_2_isConnect) {
                        mIsConnect = true;

                        BtnStatusSwitch(false, true, true);
                    }
                    Log.i(this.getClass().getName(), "BLE 已連接：" + address);
                } else if (newState == BluetoothProfile.STATE_DISCONNECTING) { // BluetoothProfile.STATE_DISCONNECTING = 3
                    // BLE 正在斷開連接
                    Log.i(this.getClass().getName(), "BLE 正在斷開連接：" + address);
                }

                try { // 間隔時間
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                dismissDialog();
            }
        });

        mBleService.setOnDataAvailableListener(new BleService.OnDataAvailableListener() { // BLE 數據回調
            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                Log.i(this.getClass().getName(), "讀取特徵（數據）");
            }

            @Override
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                if (isReadData) {
                    String address = gatt.getDevice().getAddress();
                    int place = 1;

                    for (String a : bleAddresses) {
                        if (a.equals(address)) {
                            if (a.equals(BLE_ADDRESS_PLACE1)) {
                                place = 1;
                                Log.e(this.getClass().getName(), "部位一 特徵（數據）變動" + a);
                            } else if (a.equals(BLE_ADDRESS_PLACE2)) {
                                place = 2;
                                Log.e(this.getClass().getName(), "部位二 特徵（數據）變動" + a);
                            }

                            try {
                                if (detect_status == 1) {
                                    ReadValue_before(characteristic, place);
                                } else if (detect_status == 2) {
                                    ReadValue(characteristic, place);
                                } else if (detect_status == 3) {
                                    ReadValue_after(characteristic, place);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

            }
        });
    }

    private int detect_num = 1;
    private int detect_num_1 = 0;
    private int detect_num_2 = 0;

    private void ReadValue(final BluetoothGattCharacteristic characteristic, int place) throws JSONException {
        // 數據處理
        byte[] data = characteristic.getValue();
        StringBuffer buffer = new StringBuffer();

        int i;
        String c;

        for (byte k : data) {
            i = (char) k & 0xff;
            c = Character.toString((char) i);
            buffer.append(c);
        }
        EMGdata = Integer.parseInt(buffer.toString());
        // 數據處理

        // 語音
        if (obj_array_Detect_1.length() % 60 == 0 && obj_array_Detect_2.length() % 60 == 0) {
            if (detect_num_1 < 90 && detect_num_2 < 90) {
                if (detect_num_1 == detect_num_2) {
                    Log.e("第幾次語音", String.valueOf(detect_num_1));
                    oneTothree();
                }
            }
        }
        if (place == 1) {
            if (obj_array_Detect_1.length() % 60 == 0 && obj_array_Detect_2.length() < obj_array_Detect_1.length()) { // 如果陣列一已經60筆資料，且陣列二資料數量小於陣列一，陣列一就先不存資料
//                device_1_isRead = false;
            } else {
//                device_1_isRead = true;
                if (obj_array_Detect_1.length() / 60 < 90) {
                    JSONObject obj1 = new JSONObject();
                    EMG_Data_List_detect_1.add(EMGdata);
                    obj1.put("semg", EMGdata);
                    obj1.put("count", detect_num_1 + 1);
                    if (obj1 != null) {
                        obj_array_Detect_1.put(obj1);
                    }
                }
            }
        } else {
            if (obj_array_Detect_2.length() % 60 == 0 && obj_array_Detect_1.length() < obj_array_Detect_2.length()) { // 如果陣列二已經60筆資料，且陣列一資料數量小於陣列二，陣列二就先不存資料
//                device_2_isRead = false;
            } else {
//                device_2_isRead = true;
                if (obj_array_Detect_2.length() / 60 < 90) {
                    JSONObject obj2 = new JSONObject();
                    EMG_Data_List_detect_2.add(EMGdata);
                    obj2.put("semg", EMGdata);
                    obj2.put("count", detect_num_2 + 1);
                    if (obj2 != null) {
                        obj_array_Detect_2.put(obj2);
                    }
                }
            }
        }
        if (obj_array_Detect_1.length() % 60 == 0) {
            detect_num_1 = obj_array_Detect_1.length() / 60;
            if (detect_num_1 > 90) {
                detect_num_1 = 90;
            }
        }
        if (obj_array_Detect_2.length() % 60 == 0) {
            detect_num_2 = obj_array_Detect_2.length() / 60;
            if (detect_num_2 > 90) {
                detect_num_2 = 90;
            }
        }
        if (place == 1 && obj_array_Detect_1.length() % 60 == 0 && obj_array_Detect_2.length() % 60 == 0) {
            if (detect_num_1 == 90 && detect_num_2 == 90) {
                detect_num_1 = obj_array_Detect_1.length() / 60;
                detect_num_2 = obj_array_Detect_2.length() / 60;
                Log.e("陣列1資料數量", String.valueOf(EMG_Data_List_detect_1.size()));
                Log.e("陣列2資料數量", String.valueOf(EMG_Data_List_detect_2.size()));
                Log.e("物件陣列1資料數量", String.valueOf(obj_array_Detect_1.length()));
                Log.e("物件陣列2資料數量", String.valueOf(obj_array_Detect_2.length()));
                detectEnd();
                bleServiceStopRead();
            }
        }

        addData(EMGdata, place); // 圖表

        Log.e("轉換之後的整數值 int：", String.valueOf(EMGdata));
    }

    private void ReadValue_before(final BluetoothGattCharacteristic characteristic, int place) throws JSONException {
        // 數據處理
        byte[] data = characteristic.getValue();
        StringBuffer buffer = new StringBuffer();

        int i;
        String c;

        for (byte k : data) {
            i = (char) k & 0xff;
            c = Character.toString((char) i);
            buffer.append(c);
        }
        EMGdata = Integer.parseInt(buffer.toString());
        // 數據處理

        if (place == 1 && obj_array_Before_1.length() == 2160 && obj_array_Before_2.length() == 2160) {
            Log.e("陣列1資料數量", String.valueOf(EMG_Data_List_Before_1.size()));
            Log.e("陣列2資料數量", String.valueOf(EMG_Data_List_Before_2.size()));
            Log.e("物件陣列1資料數量", String.valueOf(obj_array_Before_1.length()));
            Log.e("物件陣列2資料數量", String.valueOf(obj_array_Before_2.length()));
            detectEnd();
            bleServiceStopRead();
        }
        if (place == 1) {
            if (obj_array_Before_1.length() < 2160) {
                JSONObject obj1 = new JSONObject();
                EMG_Data_List_Before_1.add(EMGdata);
                obj1.put("semg", EMGdata);
                obj1.put("count", detect_num);
                if (obj1 != null) {
                    obj_array_Before_1.put(obj1);
                }
            }
        } else {
            if (obj_array_Before_2.length() < 2160) {
                JSONObject obj2 = new JSONObject();
                EMG_Data_List_Before_2.add(EMGdata);
                obj2.put("semg", EMGdata);
                obj2.put("count", detect_num);
                if (obj2 != null) {
                    obj_array_Before_2.put(obj2);
                }

            }
        }
        addData(EMGdata, place); // 圖表
    }

    private void ReadValue_after(final BluetoothGattCharacteristic characteristic, int place) throws JSONException {
        // 數據處理
        byte[] data = characteristic.getValue();
        StringBuffer buffer = new StringBuffer();

        int i;
        String c;

        for (byte k : data) {
            i = (char) k & 0xff;
            c = Character.toString((char) i);
            buffer.append(c);
        }
        EMGdata = Integer.parseInt(buffer.toString());
        // 數據處理

        if (obj_array_After_1.length() == 2160 && obj_array_After_2.length() == 2160) {
            Log.e("陣列1資料數量", String.valueOf(EMG_Data_List_After_1.size()));
            Log.e("陣列2資料數量", String.valueOf(EMG_Data_List_After_2.size()));
            Log.e("物件陣列1資料數量", String.valueOf(obj_array_After_1.length()));
            Log.e("物件陣列2資料數量", String.valueOf(obj_array_After_2.length()));
            detectEnd();
            bleServiceStopRead();
        }
        if (place == 1) {
            if (obj_array_After_1.length() < 2160) {
                JSONObject obj1 = new JSONObject();
                EMG_Data_List_After_1.add(EMGdata);
                obj1.put("semg", EMGdata);
                obj1.put("count", detect_num);
                if (obj1 != null) {
                    obj_array_After_1.put(obj1);
                }
            }
        } else {
            if (obj_array_After_2.length() < 2160) {
                JSONObject obj2 = new JSONObject();
                EMG_Data_List_After_2.add(EMGdata);
                obj2.put("semg", EMGdata);
                obj2.put("count", detect_num);
                if (obj2 != null) {
                    obj_array_After_2.put(obj2);
                }
            }
        }
        addData(EMGdata, place); // 圖表
    }

    public void detectemgpost_pre() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialog("上傳中");
            }
        });
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("account", Account);
            jsonObject.put("dt_time", StartTime);
            jsonObject.put("isLR", (DetectFoot.equals("左腳") ? 1 : 2));
            jsonObject.put("place", (DetectPart.equals("大腿") ? 1 : 2));
            jsonObject.put("data1", obj_array_Before_1.toString());
            jsonObject.put("data2", obj_array_Before_2.toString());

            Log.e("前測---數量1", String.valueOf(obj_array_Before_1.length()));
            Log.e("前測---數量2", String.valueOf(obj_array_Before_2.length()));
            Log.e("前測---完整物件", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        OkHttpClient copy = client.newBuilder().readTimeout(600, TimeUnit.SECONDS).build();
        final MediaType Json = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(Json, jsonObject.toString());
        Request request = new Request.Builder()
                .url(Url_Before)
                .post(body)
                .build();
        try {
            final Response response = copy.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
                Type type = new TypeToken<HttpResult<MemberMsg>>() {
                }.getType();
                final HttpResult<MemberMsg> result = gson.fromJson(resultJSON, type);
                if (result.getCode() != 500) {
                    if (result.getCode() == 0) {
//                        ToastMsg("成功");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
//                                callAltDlg("上傳成功");
                                Log.e("前測", "上傳成功");
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        detectemgpost();
                                    }
                                }).start();
                            }
                        });
                    } else if (result.getCode() == 1) {
//                        ToastMsg("code有誤");
                    }
                } else {
//                    ToastMsg("失敗");
                }
            } else {
                Log.e("前測---失敗", String.valueOf(response.isSuccessful()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void detectemgpost() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialog("上傳中");
            }
        });
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("account", Account);
            jsonObject.put("dt_time", StartTime);
            jsonObject.put("isLR", (DetectFoot.equals("左腳") ? 1 : 2));
            jsonObject.put("place", (DetectPart.equals("大腿") ? 1 : 2));
            jsonObject.put("data1", obj_array_Detect_1.toString());
            jsonObject.put("data2", obj_array_Detect_2.toString());

            Log.e("檢測---數量1", String.valueOf(obj_array_Detect_1.length()));
            Log.e("檢測---數量2", String.valueOf(obj_array_Detect_2.length()));
            Log.e("檢測---完整物件", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        OkHttpClient copy = client.newBuilder().readTimeout(600, TimeUnit.SECONDS).build();
        final MediaType Json = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(Json, jsonObject.toString());
        Request request = new Request.Builder()
                .url(Url_detect)
                .post(body)
                .build();
        try {
            final Response response = copy.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
                Type type = new TypeToken<HttpResult<MemberMsg>>() {
                }.getType();
                final HttpResult<MemberMsg> result = gson.fromJson(resultJSON, type);
                if (result.getCode() != 500) {
                    if (result.getCode() == 0) {
//                        ToastMsg("成功");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
//                                callAltDlg("上傳成功");
                                Log.e("檢測", "上傳成功");
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        detectemgpost_post();
                                    }
                                }).start();
                            }
                        });
                    } else if (result.getCode() == 1) {
//                        ToastMsg("code有誤");
                    }
                } else {
//                    ToastMsg("失敗");
                }
            } else {
                Log.e("檢測---失敗", String.valueOf(response.isSuccessful()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void detectemgpost_post() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialog("上傳中");
            }
        });
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("account", Account);
            jsonObject.put("dt_time", StartTime);
            jsonObject.put("isLR", (DetectFoot.equals("左腳") ? 1 : 2));
            jsonObject.put("place", (DetectPart.equals("大腿") ? 1 : 2));
            jsonObject.put("data1", obj_array_After_1.toString());
            jsonObject.put("data2", obj_array_After_2.toString());

            Log.e("後測---數量1", String.valueOf(obj_array_After_1.length()));
            Log.e("後測---數量2", String.valueOf(obj_array_After_2.length()));
            Log.e("後測---完整物件", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        OkHttpClient copy = client.newBuilder().readTimeout(600, TimeUnit.SECONDS).build();
        final MediaType Json = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(Json, jsonObject.toString());
        Request request = new Request.Builder()
                .url(Url_after)
                .post(body)
                .build();
        try {
            final Response response = copy.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
                Type type = new TypeToken<HttpResult<MemberMsg>>() {
                }.getType();
                final HttpResult<MemberMsg> result = gson.fromJson(resultJSON, type);
                if (result.getCode() != 500) {
                    if (result.getCode() == 0) {
//                        ToastMsg("成功");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
                                callAltDlg("上傳成功");
                                BtnUpload_switch(false);
                                Log.e("後測", "上傳成功");
                            }
                        });
                    } else if (result.getCode() == 1) {
//                        ToastMsg("code有誤");
                    }
                } else {
//                    ToastMsg("失敗");
                }
            } else {
                Log.e("檢測---失敗", String.valueOf(response.isSuccessful()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
