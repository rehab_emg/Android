package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.anastr.speedviewlib.Gauge;
import com.github.anastr.speedviewlib.Speedometer;
import com.github.anastr.speedviewlib.TubeSpeedometer;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.imd4a.ntcust.rehabemg.AnalysisActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.Model.EMGDataDetailModel;
import com.imd4a.ntcust.rehabemg.Model.UserModel;
import com.imd4a.ntcust.rehabemg.ModifyActivity;
import com.imd4a.ntcust.rehabemg.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Analysis1DataFragment extends Fragment {

    // 儀錶板
    private TubeSpeedometer rms_chart1, mf_chart1, avg_chart1, rms_chart2, mf_chart2, avg_chart2;

    // view
    private TextView isLRandPlace_View, date_View, part1_View, part2_View;

    private int grade_rms1, grade_mf1, grade_avg1, grade_rms2, grade_mf2, grade_avg2;

    private ProgressBar progressBar;
    private LinearLayout linearLayout;

    public Analysis1DataFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_analysis1_data, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar1);
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.linearlayout);

        // view
        initView();

        Bundle b = ((AnalysisActivity) getActivity()).get_1();
        if (b != null) {
            progressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);

            isLRandPlace_View.setText(b.getString("isLR_place"));
            date_View.setText(b.getString("date"));
            part1_View.setText(b.getString("part1"));
            part2_View.setText(b.getString("part2"));
        }
    }

    private void initView() {
        isLRandPlace_View = (TextView) getActivity().findViewById(R.id.isLRandPlace_View);
        date_View = (TextView) getActivity().findViewById(R.id.date_View);
        part1_View = (TextView) getActivity().findViewById(R.id.part1_View);
        part2_View = (TextView) getActivity().findViewById(R.id.part2_View);

        rms_chart1 = (TubeSpeedometer) getActivity().findViewById(R.id.rms_chart1_speed);
        mf_chart1 = (TubeSpeedometer) getActivity().findViewById(R.id.mf_chart1_speed);
        avg_chart1 = (TubeSpeedometer) getActivity().findViewById(R.id.avg_chart1_speed);

        rms_chart2 = (TubeSpeedometer) getActivity().findViewById(R.id.rms_chart2_speed);
        mf_chart2 = (TubeSpeedometer) getActivity().findViewById(R.id.mf_chart2_speed);
        avg_chart2 = (TubeSpeedometer) getActivity().findViewById(R.id.avg_chart2_speed);

        setTubeSpeedometer();
    }

    private void setTubeSpeedometer() {
        rms_chart1.setMaxSpeed(100); // 最大值
        rms_chart1.setUnitTextSize(0); // SpeedText 單位的文字大小
        rms_chart1.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        rms_chart1.setLowSpeedPercent(40);
        rms_chart1.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        rms_chart1.setSpeedometerWidth(30);
        rms_chart1.setTextSize(0);
        rms_chart1.setLowSpeedColor(Color.rgb(237, 28, 36));
        rms_chart1.setHighSpeedColor(0xFF00FF00);

        mf_chart1.setMaxSpeed(100); // 最大值
        mf_chart1.setUnitTextSize(0); // SpeedText 單位的文字大小
        mf_chart1.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        mf_chart1.setLowSpeedPercent(40);
        mf_chart1.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        mf_chart1.setSpeedometerWidth(30);
        mf_chart1.setTextSize(0);
        mf_chart1.setLowSpeedColor(Color.rgb(237, 28, 36));
        mf_chart1.setHighSpeedColor(0xFF00FF00);

        avg_chart1.setMaxSpeed(100); // 最大值
        avg_chart1.setUnitTextSize(0); // SpeedText 單位的文字大小
        avg_chart1.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        avg_chart1.setLowSpeedPercent(40);
        avg_chart1.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        avg_chart1.setSpeedometerWidth(60);
        avg_chart1.setTextSize(0);
        avg_chart1.setLowSpeedColor(Color.rgb(237, 28, 36));
        avg_chart1.setHighSpeedColor(0xFF00FF00);

        rms_chart2.setMaxSpeed(100); // 最大值
        rms_chart2.setUnitTextSize(0); // SpeedText 單位的文字大小
        rms_chart2.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        rms_chart2.setLowSpeedPercent(40);
        rms_chart2.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        rms_chart2.setSpeedometerWidth(30);
        rms_chart2.setTextSize(0);
        rms_chart2.setLowSpeedColor(Color.rgb(237, 28, 36));
        rms_chart2.setHighSpeedColor(0xFF00FF00);

        mf_chart2.setMaxSpeed(100); // 最大值
        mf_chart2.setUnitTextSize(0); // SpeedText 單位的文字大小
        mf_chart2.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        mf_chart2.setLowSpeedPercent(40);
        mf_chart2.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        mf_chart2.setSpeedometerWidth(30);
        mf_chart2.setTextSize(0);
        mf_chart2.setLowSpeedColor(Color.rgb(237, 28, 36));
        mf_chart2.setHighSpeedColor(0xFF00FF00);

        avg_chart2.setMaxSpeed(100); // 最大值
        avg_chart2.setUnitTextSize(0); // SpeedText 單位的文字大小
        avg_chart2.setSpeedTextPosition(Speedometer.Position.CENTER); // SpeedText 位置
        avg_chart2.setLowSpeedPercent(40);
        avg_chart2.setWithTremble(false); // SpeedText 抖動（若為true，到一個數值後會不停的抖）
        avg_chart2.setSpeedometerWidth(60);
        avg_chart2.setTextSize(0);
        avg_chart2.setLowSpeedColor(Color.rgb(237, 28, 36));
        avg_chart2.setHighSpeedColor(0xFF00FF00);
    }

    public void setView(String isLR, String place, String date, String part1, int avg_rms1, int avg_mf1, int avg1, String part2, int avg_rms2, int avg_mf2, int avg2) {
        isLRandPlace_View.setText(isLR + " " + place);
        date_View.setText(date);
        part1_View.setText("檢測肌肉：" + part1);
        part2_View.setText("檢測肌肉：" + part2);

        grade_rms1 = avg_rms1;
        grade_mf1 = avg_mf1;
        grade_avg1 = avg1;

        rms_chart1.speedTo(grade_rms1);
        mf_chart1.speedTo(grade_mf1);
        avg_chart1.speedTo(grade_avg1);

        grade_rms2 = avg_rms2;
        grade_mf2 = avg_mf2;
        grade_avg2 = avg2;

        rms_chart2.speedTo(grade_rms2);
        mf_chart2.speedTo(grade_mf2);
        avg_chart2.speedTo(grade_avg2);

        progressBar.setVisibility(View.INVISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
