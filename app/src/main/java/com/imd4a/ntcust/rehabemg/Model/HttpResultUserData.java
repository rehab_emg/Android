package com.imd4a.ntcust.rehabemg.Model;

import java.util.Date;

public class HttpResultUserData {
    private String account;
    private String name;
    private String cellphone;
    private String email;
    private String img;
    private String birthday;
    private int sex;

    public String getAccount() { return  account; }

    public String getName() { return  name; }

    public String getBirthday() { return  birthday; }

    public String getCellphone() { return  cellphone; }

    public String getEmail() { return  email; }

    public int getSex() { return  sex; }

    public String getImg() { return img; }
}
