package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.imd4a.ntcust.rehabemg.MainClass.RegisterMetadata;
import com.imd4a.ntcust.rehabemg.ModifyActivity;
import com.imd4a.ntcust.rehabemg.R;
import com.imd4a.ntcust.rehabemg.RegisterActivity;
import com.imd4a.ntcust.rehabemg.WebApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordModifyFragment extends Fragment {

    private WebApi webApi = new WebApi();
    private String ChangePasswordUrl = webApi.GetChangePasswordApi();

    private RegisterMetadata registerMetadata = new RegisterMetadata(); // 註冊驗證資料

    private TextInputLayout TxtLayoutPassword, TxtLayoutNewPassword, TxtLayoutNewPasswordCheck;
    private EditText EdtPassword, EdtNewPassword, EdtNewPasswordCheck;
    private Button BtnSend;

    private Bundle bundle;

    private boolean bPassword, bNewPassword, bNewPasswordCheck;

    public PasswordModifyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        View v = inflater.inflate(R.layout.fragment_analysis1_data, container, false);

        return inflater.inflate(R.layout.fragment_password_modify, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bundle = ((ModifyActivity) getActivity()).bundle;

        initView();
        SetListener();
    }

    private void initView() {
        TxtLayoutPassword = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutPassword);
        TxtLayoutNewPassword = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutNewPassword);
        TxtLayoutNewPasswordCheck = (TextInputLayout) getActivity().findViewById(R.id.txtLayoutNewPasswordCheck);

        EdtPassword = (EditText) getActivity().findViewById(R.id.edtPassword);
        EdtNewPassword = (EditText) getActivity().findViewById(R.id.edtNewPassword);
        EdtNewPasswordCheck = (EditText) getActivity().findViewById(R.id.edtNewPasswordCheck);

        BtnSend = (Button) getActivity().findViewById(R.id.btnSend);
    }

    private void SetListener() {
        // 送出資料
        BtnSend.setOnClickListener(BtnSendOnClick);

        // TextInputLayout 動態監聽
        EdtPassword.addTextChangedListener(new textListener(EdtPassword));
        EdtNewPassword.addTextChangedListener(new textListener(EdtNewPassword));
        EdtNewPasswordCheck.addTextChangedListener(new textListener(EdtNewPasswordCheck));
    }

    // 送出資料
    private View.OnClickListener BtnSendOnClick = new View.OnClickListener() {
        public void onClick(View view) {
//            EdtPassword.getText().toString();
//            EdtNewPassword.getText().toString();
//            EdtNewPasswordCheck.getText().toString();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    ChangePassword();
                }
            }).start();
        }
    };

    //  修改密碼API
    public void ChangePassword() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", bundle.getString("account"));
            jsonObject.put("password", EdtPassword.getText().toString());
            jsonObject.put("newPassword", EdtNewPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(ChangePasswordUrl)
                .post(body)
                .build();
        try {
            final Response response = client.newCall(request).execute();//response.getStatusLine().getStatusCode()
            if (response.isSuccessful()) {
                if(response.code() == 200){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(((ModifyActivity) getActivity()))
                                    .setTitle("訊息")
                                    .setMessage("修改成功 !")
                                    .setCancelable(false)
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
//                            Toast.makeText(getActivity(), "成功", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }else{
                if(response.code() == 401){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(((ModifyActivity) getActivity()))
                                    .setTitle("訊息")
                                    .setMessage("舊密碼輸入錯誤 !")
                                    .setCancelable(false)
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
//                            Toast.makeText(getActivity(), "舊密碼輸入錯誤", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else if(response.code() == 403){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(((ModifyActivity) getActivity()))
                                    .setTitle("訊息")
                                    .setMessage("輸入資料格式錯誤 !")
                                    .setCancelable(false)
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
//                            Toast.makeText(getActivity(), "輸入資料格式錯誤", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity(), "失敗", Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // EditText動態監聽 驗證資料
    private class textListener implements TextWatcher {
        private View view;

        private textListener(View v) {
            this.view = v;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String s;

            // 資料驗證
            switch (view.getId()) {
                case R.id.edtPassword:
                    s = registerMetadata.PasswordValidate(EdtPassword.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutPassword.setErrorEnabled(false);
                        bPassword = true;
                    } else {
                        TxtLayoutPassword.setErrorEnabled(true);
                        TxtLayoutPassword.setError(s);
                        bPassword = false;
                    }
                    break;
                case R.id.edtNewPassword:
                    s = registerMetadata.PasswordValidate(EdtNewPassword.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutNewPassword.setErrorEnabled(false);
                        bNewPassword = true;
                    } else {
                        TxtLayoutNewPassword.setErrorEnabled(true);
                        TxtLayoutNewPassword.setError(s);
                        bNewPassword = false;
                    }
                    break;
                case R.id.edtNewPasswordCheck:
                    s = registerMetadata.NewPasswordCheckValidate(EdtNewPassword.getText().toString(), EdtNewPasswordCheck.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutNewPasswordCheck.setErrorEnabled(false);
                        bNewPasswordCheck = true;
                    } else {
                        TxtLayoutNewPasswordCheck.setErrorEnabled(true);
                        TxtLayoutNewPasswordCheck.setError(s);
                        bNewPasswordCheck = false;
                    }
                    break;
            }
        }
    }
}
