package com.imd4a.ntcust.rehabemg.Model;

import java.util.ArrayList;

public class HttpResultEMGData {
    private String isLR;
    private String place;
    private String date;
    private String part;
    private float[] rms;
    private float[] mf;
    private float[] mpf;
    private ArrayList<Integer> data;

    private int avg_rms;
    private int avg_mf;
    private int avg;

    public String getIsLR() {
        return isLR;
    }

    public String getPlace() {
        return place;
    }

    public String getDate() {
        return date;
    }

    public String getPart() {
        return part;
    }

    public float[] getRms() {
        return rms;
    }

    public float[] getMf() {
        return mf;
    }

    public float[] getMpf() {
        return mpf;
    }

    public ArrayList<Integer> getData() {
        return data;
    }

    public int getAvg_rms() {
        return avg_rms;
    }

    public int getAvg_mf() {
        return avg_mf;
    }

    public int getAvg() {
        return avg;
    }
}
