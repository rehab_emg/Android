package com.imd4a.ntcust.rehabemg;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Calendar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imd4a.ntcust.rehabemg.MainClass.RegisterMetadata;
import com.imd4a.ntcust.rehabemg.Model.RegisterModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    private WebApi webApi = new WebApi();
    private String Url = webApi.GetRegisterApi();

    private RegisterMetadata registerMetadata = new RegisterMetadata(); // 註冊驗證資料
    private JSONObject json = new JSONObject();
    private Toolbar toolbar;
    private MemberData memberData = MemberData.getInstance();
    private TextInputLayout TxtLayoutAccount, TxtLayoutPassword, TxtLayoutPasswordCheck, TxtLayoutName, TxtLayoutCellphone, TxtLayoutEmail;
    private EditText EdtAccount, EdtPassword, EdtPasswordCheck, EdtName, EdtCellphone, EdtEmail, EdtBirthday;
    private Button BtnSend;
    private RadioGroup RadGrpSex;
    private RadioButton RadBtnMale, RadBtnFemale;
    private ImageView ImgViewBirth;
    private String PasswordCheck, RegisterAccount, RegisterPassword, RegisterName, RegisterEmail, RegisterCellphone, RegisterBirthday;
    private int Sex, RegisterSex;

    private boolean bAccount, bPassword, bPasswordCheck, bName, bCellphone, bEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // 初始 View
        InitView();

        // 設置監聽事件
        SetListener();

        // 加入 ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 加入 ActionBar 返回鍵
        ActionBar act = getSupportActionBar();
        act.setDisplayHomeAsUpEnabled(true);

        // 進入 Activity 時，鍵盤不自動彈出
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // 初始 View
    private void InitView() {
        TxtLayoutAccount = (TextInputLayout) findViewById(R.id.txtLayoutAccount);
        TxtLayoutPassword = (TextInputLayout) findViewById(R.id.txtLayoutPassword);
        TxtLayoutPasswordCheck = (TextInputLayout) findViewById(R.id.txtLayoutPasswordCheck);
        TxtLayoutName = (TextInputLayout) findViewById(R.id.txtLayoutName);
        TxtLayoutCellphone = (TextInputLayout) findViewById(R.id.txtLayoutCellphone);
        TxtLayoutEmail = (TextInputLayout) findViewById(R.id.txtLayoutEmail);

        EdtAccount = (EditText) findViewById(R.id.edtAccount);
        EdtPassword = (EditText) findViewById(R.id.edtPassword);
        EdtPasswordCheck = (EditText) findViewById(R.id.edtPasswordCheck);
        EdtName = (EditText) findViewById(R.id.edtName);
        EdtCellphone = (EditText) findViewById(R.id.edtCellphone);
        EdtEmail = (EditText) findViewById(R.id.edtEmail);
        EdtBirthday = (EditText) findViewById(R.id.edtBirthday);
        EdtBirthday.setInputType(InputType.TYPE_NULL);

        BtnSend = (Button) findViewById(R.id.btnSend);

        RadGrpSex = (RadioGroup) findViewById(R.id.radGrpSex);
        RadBtnMale = (RadioButton) findViewById(R.id.radBtnMale);
        RadBtnFemale = (RadioButton) findViewById(R.id.radBtnFemale);

        ImgViewBirth = (ImageView) findViewById(R.id.imgViewBirth);
    }

    private void SetListener() {
        // 選擇出生日期
        ImgViewBirth.setOnClickListener(ImgViewBirthOnClick);
//        EdtBirthday.setOnClickListener(ImgViewBirthOnClick);
        // 點擊日期EditText不彈出鍵盤
        EdtBirthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                EdtBirthday.setInputType(InputType.TYPE_NULL);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    ImgViewBirth.callOnClick();
                }
                return true;
            }
        });

        // 選擇性別
        RadGrpSex.setOnCheckedChangeListener(RadGrpSexCheckedChange);

        // 送出資料
        BtnSend.setOnClickListener(BtnSendOnClick);

        // TextInputLayout 動態監聽
        EdtAccount.addTextChangedListener(new textListener(EdtAccount));
        EdtPassword.addTextChangedListener(new textListener(EdtPassword));
        EdtPasswordCheck.addTextChangedListener(new textListener(EdtPasswordCheck));
        EdtName.addTextChangedListener(new textListener(EdtName));
        EdtCellphone.addTextChangedListener(new textListener(EdtCellphone));
        EdtEmail.addTextChangedListener(new textListener(EdtEmail));
    }

    // EditText動態監聽 驗證資料
    private class textListener implements TextWatcher {
        private View view;

        private textListener(View v) {
            this.view = v;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String s;

            // 資料驗證
            switch (view.getId()) {
                case R.id.edtAccount:
                    s = registerMetadata.AccountValidate(EdtAccount.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutAccount.setErrorEnabled(false);
                        bAccount = true;
                    } else {
                        TxtLayoutAccount.setErrorEnabled(true);
                        TxtLayoutAccount.setError(s);
                        bAccount = false;
                    }
                    break;
                case R.id.edtPassword:
                    s = registerMetadata.PasswordValidate(EdtPassword.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutPassword.setErrorEnabled(false);
                        bPassword = true;
                    } else {
                        TxtLayoutPassword.setErrorEnabled(true);
                        TxtLayoutPassword.setError(s);
                        bPassword = false;
                    }
                    break;
                case R.id.edtPasswordCheck:
                    s = registerMetadata.PasswordCheckValidate(EdtPassword.getText().toString(), EdtPasswordCheck.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutPasswordCheck.setErrorEnabled(false);
                        bPasswordCheck = true;
                    } else {
                        TxtLayoutPasswordCheck.setErrorEnabled(true);
                        TxtLayoutPasswordCheck.setError(s);
                        bPasswordCheck = false;
                    }
                    break;
                case R.id.edtName:
                    s = registerMetadata.NameValidate(EdtName.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutName.setErrorEnabled(false);
                        bName = true;
                    } else {
                        TxtLayoutName.setErrorEnabled(true);
                        TxtLayoutName.setError(s);
                        bName = false;
                    }
                    break;
                case R.id.edtCellphone:
                    s = registerMetadata.CellphoneValidate(EdtCellphone.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutCellphone.setErrorEnabled(false);
                        bCellphone = true;
                    } else {
                        TxtLayoutCellphone.setErrorEnabled(true);
                        TxtLayoutCellphone.setError(s);
                        bCellphone = false;
                    }
                    break;
                case R.id.edtEmail:
                    s = registerMetadata.EmailValidate(EdtEmail.getText().toString());
                    if (s.equals("正確")) {
                        TxtLayoutEmail.setErrorEnabled(false);
                        bEmail = true;
                    } else {
                        TxtLayoutEmail.setErrorEnabled(true);
                        TxtLayoutEmail.setError(s);
                        bEmail = false;
                    }
                    break;
            }
        }
    }

    // 選擇出生日期
    private View.OnClickListener ImgViewBirthOnClick = new View.OnClickListener() {
        public void onClick(View view) {
            // 取得日曆時間
            Calendar now = Calendar.getInstance();

            // 用Dialog視窗選擇
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    RegisterActivity.this,
                    datePickerDialogSet,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));

            // 設定Dialog視窗屬性
            datePickerDialog.setTitle("選擇日期");
            datePickerDialog.setMessage("選擇您的出生日期");
            datePickerDialog.setIcon(android.R.drawable.ic_dialog_info);
            datePickerDialog.setCancelable(true); // 點擊對話窗外區域關閉Dialog

            datePickerDialog.show();
        }
    };

    // 選擇日期 Dialog視窗
    private DatePickerDialog.OnDateSetListener datePickerDialogSet = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int day) {
            EdtBirthday.setText(String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(day));
        }
    };

    // 性別選擇
    private RadioGroup.OnCheckedChangeListener RadGrpSexCheckedChange = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.radBtnMale:
                    Sex = 1;
                    break;
                case R.id.radBtnFemale:
                    Sex = 0;
                    break;
            }
        }
    };

    // 註冊 送出資料
    private View.OnClickListener BtnSendOnClick = new View.OnClickListener() {
        public void onClick(View view) {
            RegisterModel data = new RegisterModel();

            data.Account = EdtAccount.getText().toString();
            data.Password = EdtPassword.getText().toString();
            PasswordCheck = EdtPasswordCheck.getText().toString();
            data.Name = EdtName.getText().toString();
            data.Birthday = EdtBirthday.getText().toString();
            data.Cellphone = EdtCellphone.getText().toString();
            data.Email = EdtEmail.getText().toString();
            data.Sex = Sex;

            RegisterAccount = EdtAccount.getText().toString();
            RegisterPassword = EdtPassword.getText().toString();
            RegisterName = EdtName.getText().toString();
            RegisterBirthday = EdtBirthday.getText().toString();
            RegisterCellphone = EdtCellphone.getText().toString();
            RegisterEmail = EdtEmail.getText().toString();
            RegisterSex = Sex;

            try {
                json.put("account", RegisterAccount);
                json.put("password", RegisterPassword);
                json.put("name", RegisterName);
                json.put("birthday", RegisterBirthday);
                json.put("cellphone", RegisterCellphone);
                json.put("email", RegisterEmail);
                json.put("sex", RegisterSex);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (bAccount && bPassword && bPasswordCheck && bName && bCellphone && bEmail) {
                Toast.makeText(RegisterActivity.this, "註冊成功", Toast.LENGTH_LONG).show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Register();
                    }
                }).start();
                // 送出註冊資料
            } else {
                Toast.makeText(RegisterActivity.this, "資料未填寫完成", Toast.LENGTH_LONG).show();
            }
        }
    };

    public void Register() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", RegisterAccount);
            jsonObject.put("password", RegisterPassword);
            jsonObject.put("name", RegisterName);
            jsonObject.put("email", RegisterEmail);
            jsonObject.put("cellphone", RegisterCellphone);
            jsonObject.put("birthday", RegisterBirthday);
            jsonObject.put("sex", RegisterSex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(Url)
                .post(body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
                Type type = new TypeToken<HttpResult<MemberMsg>>() {
                }.getType();
                final HttpResult<MemberMsg> result = gson.fromJson(resultJSON, type);
                if (result.getCode() != 500) {
                    if (result.getCode() == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(RegisterActivity.this)
                                        .setTitle("註冊訊息")
                                        .setMessage("註冊成功！請去收信驗證！\r\n並請至APP登入頁面進入驗證畫面輸入驗證碼！")
                                        .setCancelable(false)
                                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                memberData.setMemberMsg(result.getData());
                                                RegisterActivity.this.finish();
                                            }
                                        })
                                        .show();
                            }
                        });

                    } else if (result.getCode() == 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(RegisterActivity.this)
                                        .setTitle("註冊訊息")
                                        .setMessage("註冊失敗 !")
                                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        })
                                        .show();
                                EdtAccount.setText("");
                                EdtPassword.setText("");
                            }
                        });

                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(RegisterActivity.this)
                                    .setTitle("註冊訊息")
                                    .setMessage("錯誤")
                                    .show();
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
