package com.imd4a.ntcust.rehabemg;

public class MemberData {
    private MemberMsg memberMsg = null;
    private static class MemberData2{
        private static MemberData memberData = new MemberData();
    }
    private MemberData(){

    }

    public MemberMsg getMemberMsg() {
        return memberMsg;
    }

    public void setMemberMsg(MemberMsg memberMsg) {
        this.memberMsg = memberMsg;
    }

    public static MemberData getInstance(){
        return MemberData2.memberData;
    }
}
