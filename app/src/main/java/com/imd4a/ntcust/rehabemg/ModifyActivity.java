package com.imd4a.ntcust.rehabemg;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.imd4a.ntcust.rehabemg.Fragment.PasswordModifyFragment;
import com.imd4a.ntcust.rehabemg.Fragment.UserModifyFragment;
import com.imd4a.ntcust.rehabemg.adapter.AnalysisFragmenPagerAdapter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ModifyActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {

    private String user_account, user_name, user_cellphone, user_email;

    private ViewPager viewPager;
    private TabHost tabHost;
    private TabWidget tabWidget;

    private UserModifyFragment userModifyFragment;
    private PasswordModifyFragment passwordModifyFragment;

    public Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 保持喚醒

        Intent it = getIntent();
        bundle = it.getExtras();

        user_account = bundle.getString("account");
        user_name = bundle.getString("name");
        user_cellphone = bundle.getString("cellphone");
        user_email = bundle.getString("email");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("修改個人資料");

        initFragment();
        initViewPager();
        initTabHost();
        initTabWidget();
    }

    private void initFragment() {
        userModifyFragment = new UserModifyFragment();
        passwordModifyFragment = new PasswordModifyFragment();
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        List<Fragment> listFragment = new ArrayList<Fragment>();
        listFragment.add(userModifyFragment);
        listFragment.add(passwordModifyFragment);

        AnalysisFragmenPagerAdapter analysisFragmenPagerAdapter = new AnalysisFragmenPagerAdapter(getSupportFragmentManager(), listFragment);

        viewPager.setAdapter(analysisFragmenPagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    private void initTabHost() {
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        String[] tabNames = {"修改資料", "修改密碼"};

        for (int i = 0; i < tabNames.length; i++) {
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator(tabNames[i]);
            tabSpec.setContent(new FakeContent(getApplicationContext()));
            tabHost.addTab(tabSpec);
        }

        tabHost.setOnTabChangedListener(this);
    }

    private void initTabWidget() {
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        for (int i = 0; i < 2; i++) {
            TextView tx = tabWidget.getChildTabViewAt(i).findViewById(android.R.id.title);
            tx.setTextSize(16);
        }
    }

    public class FakeContent implements TabHost.TabContentFactory {

        Context context;

        public FakeContent(Context mcontext) {
            this.context = mcontext;
        }

        @Override
        public View createTabContent(String tag) {

            View fakeView = new View(context);
            fakeView.setMinimumHeight(0);
            fakeView.setMinimumWidth(0);
            return fakeView;
        }
    }

    // viewPager Listener
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    // tabHost Listener
    @Override
    public void onTabChanged(String tabId) {
        int selectedPage = tabHost.getCurrentTab();
        viewPager.setCurrentItem(selectedPage);
    }
}
