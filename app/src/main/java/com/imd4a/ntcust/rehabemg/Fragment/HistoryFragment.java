package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.SimpleExpandableListAdapter;

import com.google.gson.Gson;
import com.imd4a.ntcust.rehabemg.AnalysisActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.Model.HttpResultHistory;
import com.imd4a.ntcust.rehabemg.R;
import com.imd4a.ntcust.rehabemg.WebApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener {

    private WebApi webApi = new WebApi();
    private String historyUrl = webApi.GetDetectionTimesApi();

    private ExpandableListView expandableListView;
    private ProgressBar progressBar;

    private JSONArray historyData;
    private ArrayList<String> detection_date_data = new ArrayList<String>();
    private ArrayList<Integer> detection_date_data_count = new ArrayList<Integer>();
    private ArrayList<String> detection_id_head = new ArrayList<String>();

    private ArrayList<ArrayList<String>> detection_isLR_place = new ArrayList<ArrayList<String>>();

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("onCreate", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("onCreateView", "onCreateView");
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("onActivityCreated", "onActivityCreated");

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("歷史紀錄");

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Historyinfo();
            }
        }).start();
    }

    private void initList() {
        expandableListView = (ExpandableListView) getActivity().findViewById(R.id.expandableListView);

        List<Map<String, String>> date_list = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> date_times_list = new ArrayList<List<Map<String, String>>>();

        for (int i = 0; i < detection_date_data.size(); i++) {
            Map<String, String> date_data = new HashMap<String, String>();
            date_data.put("item_date", detection_date_data.get(i));
            date_list.add(date_data);

            List<Map<String, String>> date_times = new ArrayList<Map<String, String>>();
            for (int j = 0; j < detection_date_data_count.get(i); j++) {
                Map<String, String> date_times_data = new HashMap<String, String>();
                date_times_data.put("item_date_times", detection_date_data.get(i) + "-" + String.valueOf(j + 1));
                date_times_data.put("item_date_part", detection_isLR_place.get(i).get(j));
                date_times.add(date_times_data);
            }
            date_times_list.add(date_times);
        }

        ExpandableListAdapter expandableListAdapter = new SimpleExpandableListAdapter(
                ((MainActivity) getActivity()),
                date_list,
                android.R.layout.simple_expandable_list_item_1,
                new String[]{"item_date"},
                new int[]{android.R.id.text1, android.R.id.text2},
                date_times_list,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{"item_date_times", "item_date_part"},
                new int[]{android.R.id.text1, android.R.id.text2}
        );

        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnChildClickListener(this);
        expandableListView.setOnGroupClickListener(this);

        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        String times = (childPosition + 1 < 10) ? "0" + String.valueOf(childPosition + 1) : String.valueOf(childPosition + 1);
        String detection_id = detection_id_head.get(groupPosition) + "-" + ((MainActivity) getActivity()).userAccount + "-" + times;

        Log.e("IDIDIDIDIDIDIDIDID", detection_id);

        Bundle bundle = new Bundle();
        bundle.putString("detection_id", detection_id);
        Intent it = new Intent((MainActivity) getActivity(), AnalysisActivity.class);
        it.putExtras(bundle);
        startActivity(it);
        return true;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return false;
    }

    public void Historyinfo(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", ((MainActivity) getActivity()).userAccount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        OkHttpClient copy = client.newBuilder().readTimeout(60, TimeUnit.SECONDS).build();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(historyUrl)
                .post(body)
                .build();
        try {
            final Response response = copy.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                historyData = new JSONArray(resultJSON);
                Log.e("ArrayArrayArrayArray", historyData.toString());

                for (int i = 0; i < historyData.length(); i++) {
                    Gson gson = new Gson();
                    HttpResultHistory httpResultHistory = gson.fromJson(historyData.getJSONObject(i).toString(), HttpResultHistory.class);


//                    JSONObject obj = historyData.getJSONObject(i);
                    String year = httpResultHistory.getYy();
                    String month = httpResultHistory.getMm();
                    String day = httpResultHistory.getDd();
                    int count = httpResultHistory.getCount();

                    ArrayList<String> isLR_array = httpResultHistory.getIsLR();
                    ArrayList<String> place_array = httpResultHistory.getPlace();

                    ArrayList<String> isLR_place_array = new ArrayList<String>();
                    for(int j = 0; j < isLR_array.size(); j++){
                        isLR_place_array.add(isLR_array.get(j) + " " + place_array.get(j));
                    }
                    detection_isLR_place.add(isLR_place_array);

                    detection_date_data.add(year + "-" + month + "-" + day);
                    detection_date_data_count.add(count);

                    detection_id_head.add(year + month + day);
                }

                ((MainActivity)getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initList();
                    }
                });
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }
}
