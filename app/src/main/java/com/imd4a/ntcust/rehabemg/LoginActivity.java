package com.imd4a.ntcust.rehabemg;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.session.MediaSession;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imd4a.ntcust.rehabemg.Model.HttpResultUserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    private WebApi webApi = new WebApi();
    private String Url = webApi.GetLoginApi();
    private String userUrl = webApi.GetUserInfoApi();

    private String LoginAccount, LoginPassword, usertoken, userimg, useremail;
    public static final MediaType POST = MediaType.parse("application/x-www-form-urlencoded");
    private JSONObject json = new JSONObject();
    private Button BtnLogin, BtnCreateUser, BtnValidate;
    private EditText EdtAccount, EdtPassword;
    private MemberData memberData = MemberData.getInstance();
    private ProgressDialog progressDialog;
    private MainActivity mainActivity;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //找暫存檔，若有登入則跳過登入畫面
        if (getSharedPreferences("data", MODE_PRIVATE) != null) {
            boolean a = getSharedPreferences("data", MODE_PRIVATE).getBoolean("isLogin", false);
            if (a == true) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        }

        mainActivity = new MainActivity();

        // 初始 View
        InitView();

        // 設置監聽事件
        SetListener();

        // 設置Login中的dialog
        SetLoginDialog();

        // 進入 Activity 時，鍵盤不自動彈出
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void SetLoginDialog() {
        if(progressDialog == null){
            progressDialog = new ProgressDialog(LoginActivity.this);
        }
        progressDialog.setMessage("登入中");
    }

    // 初始 View
    private void InitView() {
        BtnLogin = (Button) findViewById(R.id.btnLogin);
        BtnCreateUser = (Button) findViewById(R.id.btnCreateUser);
        BtnValidate = (Button) findViewById(R.id.btnValidate);
        EdtAccount = (EditText) findViewById(R.id.edtAccount);
        EdtPassword = (EditText) findViewById(R.id.edtPassword);
    }

    // 設置監聽事件
    private void SetListener() {
        BtnLogin.setOnClickListener(BtnLoginOnClick);
        BtnCreateUser.setOnClickListener(BtnCreateUserOnClick);
        BtnValidate.setOnClickListener(BtnValidateOnClick);
    }

    // 登入
    private View.OnClickListener BtnLoginOnClick = new View.OnClickListener() {
        public void onClick(View v) {
//            String ValidateStr = "成功";

            LoginAccount = EdtAccount.getText().toString();
            LoginPassword = EdtPassword.getText().toString();
            try {
                json.put("account", LoginAccount);
                json.put("password", LoginPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!LoginAccount.isEmpty() && !LoginPassword.isEmpty()) {
                // 接API、資料驗證
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Login();
                    }
                }).start();
            } else {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("訊息")
                        .setMessage("帳號密碼不可為空！")
                        .setCancelable(false)
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }
        }
    };

    // 註冊
    private View.OnClickListener BtnCreateUserOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener BtnValidateOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, ValidateActivity.class);
            startActivity(intent);
        }
    };

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
            if (msg.what == 0) { // 驗證通過
                // 使用SharedPreferences物件儲存使用者登入資料
                sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                SharedPreferences.Editor setData = sharedPreferences.edit();
                setData.putString("Account", LoginAccount);
                setData.putString("Password", LoginPassword);
                setData.putBoolean("isLogin", true);
                setData.commit();
                // 或
                // sharedPreferences.edit().putString("Account", LoginAccount).putString("Password", LoginPassword).apply();

                // 清空EditText
                EdtAccount.setText("");
                EdtPassword.setText("");

                Intent userLogin = new Intent();
                userLogin.setClass(LoginActivity.this, MainActivity.class);
                progressDialog.dismiss(); // 關閉ProgressDialog

                startActivity(userLogin);
            } else if (msg.what == 404) { // 驗證失敗
                progressDialog.dismiss(); // 關閉ProgressDialog
                Toast.makeText(LoginActivity.this, "登入失敗", Toast.LENGTH_LONG).show();
            }
        }
    };

    public void Login() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", LoginAccount);
            jsonObject.put("password", LoginPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType Json = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(Json, jsonObject.toString());
        Request request = new Request.Builder()
                .url(Url)
                .post(body)
                .build();
        try {
            final Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                Userinfo();
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
                Type type = new TypeToken<HttpResult<MemberMsg>>() {
                }.getType();
                final HttpResult<MemberMsg> result = gson.fromJson(resultJSON, type);
                if (result.getCode() != 500) {
                    if (result.getCode() == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                new AlertDialog.Builder(LoginActivity.this)
                                        .setTitle("登入訊息")
                                        .setMessage("登入成功 !")
                                        .setCancelable(false)
                                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                memberData.setMemberMsg(result.getData());
                                                usertoken = result.getToken();
                                                sharedPreferences = getSharedPreferences("data", MODE_PRIVATE); //站存檔
                                                SharedPreferences.Editor setData = sharedPreferences.edit();
                                                setData.putString("Account", LoginAccount);
                                                setData.putBoolean("isLogin", true);
                                                setData.putString("Token", usertoken);
                                                setData.putString("Img", userimg);
                                                setData.putString("Email", useremail);
                                                setData.commit(); // or commit()
                                                Intent intent = new Intent();
                                                intent.setClass(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                LoginActivity.this.finish();
                                            }
                                        }).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("登入訊息")
                                    .setMessage("登入階段錯誤")
                                    .show();
                        }
                    });
                }
            } else {
                progressDialog.dismiss();
                if (response.code() == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("登入失敗")
                                    .setMessage("帳號或密碼錯誤 !")
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    });
                }
                if (response.code() == 402) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("登入失敗")
                                    .setMessage("信箱尚未驗證！")
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    });
                }
                if (response.code() == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("登入失敗")
                                    .setMessage("帳號或密碼輸入格式錯誤！")
                                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

            progressDialog.dismiss();
            new AlertDialog.Builder(LoginActivity.this)
                    .setTitle("訊息")
                    .setMessage("連線逾時！請確認網路狀態後再次登入！")
                    .setCancelable(false)
                    .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
    }

    //  用個人資料API抓email
    public void Userinfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", LoginAccount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(userUrl)
                .post(body)
                .build();
//                .addHeader("Authorization", usertoken)
        try {
            final Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final Gson gson = new Gson();
//                Type type = new TypeToken<HttpResult<MemberMsg>>() {
//                }.getType();
                HttpResultUserData result = gson.fromJson(resultJSON, HttpResultUserData.class);
                useremail = result.getEmail();
                userimg = result.getImg();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
