package com.imd4a.ntcust.rehabemg.Model;

import java.util.ArrayList;

public class EMGDataDetailModel {
    public String isLR;
    public String place;
    public String date;
    public String part;
    public float[] rms;
    public float[] mf;
    public float[] mpf;
    public ArrayList<Integer> semg;
    public int avg_rms;
    public int avg_mf;
    public int avg = -1;
}
