package com.imd4a.ntcust.rehabemg;

public class MemberMsg {
    private String comp_No;
    private String account;
    private String name;

    public String getComp_No() {
        return comp_No;
    }

    public void setComp_No(String comp_No) {
        this.comp_No = comp_No;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
