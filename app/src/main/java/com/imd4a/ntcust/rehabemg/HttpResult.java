package com.imd4a.ntcust.rehabemg;

import java.util.Date;

public class HttpResult<T> {
    private int code;
    private String message;
    private String token;
    private String yy, mm, dd;
    private int count;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getToken() { return token; }

    //historyrecord
    public String getYy() { return yy; }
    public String getMm() { return mm; }
    public String getDd() { return dd; }
    public int getCount() { return count; }
}
