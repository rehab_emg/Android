package com.imd4a.ntcust.rehabemg.Model;

import java.util.ArrayList;

public class HttpResultHistory {
    private String yy;
    private String mm;
    private String dd;
    private int count;
    private ArrayList<String> isLR;
    private ArrayList<String> place;

    public String getYy() {
        return yy;
    }

    public String getMm(){
        return mm;
    }

    public String getDd(){
        return dd;
    }

    public int getCount(){
        return count;
    }

    public ArrayList<String> getIsLR(){
        return isLR;
    }

    public ArrayList<String> getPlace() {
        return place;
    }
}
