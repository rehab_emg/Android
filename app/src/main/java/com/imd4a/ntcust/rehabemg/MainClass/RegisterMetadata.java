package com.imd4a.ntcust.rehabemg.MainClass;

import android.util.Patterns;

public class RegisterMetadata {
//    private String Account, Password, PasswordCheck, Name, Email, Cellphone;

    public String AccountValidate(String Account) {
        if (Account.equals("")) {
            return "請輸入帳號";
        } else if (Account.length() < 6) {
            return "帳號不可少於6字元";
        } else if (false) {
            // 驗證是否註冊區塊
            return "帳號已註冊";
        } else {
            return "正確";
        }
    }

    public String PasswordValidate(String Password) {
        if (Password.equals("")) {
            return "請輸入密碼";
        } else if (Password.length() < 6) {
            return "密碼不可少於6字元";
        } else {
            return "正確";
        }
    }

    public String PasswordCheckValidate(String Password, String PasswordCheck) {
        if (PasswordCheck.equals("")) {
            return "請輸入確認密碼";
        } else if (!PasswordCheck.equals(Password)) {
            return "與密碼不符";
        } else {
            return "正確";
        }
    }

    public String NameValidate(String Name) {
        if (Name.equals("")) {
            return "請輸入姓名";
        } else {
            return "正確";
        }
    }

    public String CellphoneValidate(String Cellphone) {
        if (Cellphone.equals("")) {
            return "請輸入電話";
        } else if (Cellphone.length() < 10) {
            return "電話格式不正確";
        } else {
            return "正確";
        }
    }

    public String EmailValidate(String Email) {
        if (Email.equals("")) {
            return "請輸入Email";
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            return "Email格式不正確";
        } else {
            return "正確";
        }
    }

    public String NewPasswordCheckValidate(String Password, String PasswordCheck) {
        if (PasswordCheck.equals("")) {
            return "請輸入確認密碼";
        } else if (!PasswordCheck.equals(Password)) {
            return "與新密碼不符";
        } else {
            return "正確";
        }
    }
}
