package com.imd4a.ntcust.rehabemg;

public class WebApi {
    private String Url = "http://8th2.hsclab.nutc.edu.tw/";

    // member
    private String UserInfo = "api/member/user";
    private String Login = "api/member/login";
    private String Register = "api/appmember/register";
    private String ChangePassword = "api/appmember/changepassword";
    private String ChangeOther = "api/appmember/changeother";
    private String RegisterValidate = "api/appmember/emailvalidate";

    public String GetUserInfoApi() {
        return Url + UserInfo;
    }

    public String GetLoginApi() {
        return Url + Login;
    }

    public String GetRegisterApi() {
        return Url + Register;
    }

    public String GetChangePasswordApi() {
        return Url + ChangePassword;
    }

    public String GetChangeOtherApi() {
        return Url + ChangeOther;
    }

    public String GetRegisterValidateApi(){
        return Url + RegisterValidate;
    }

    // AppEmg
    private String Emg_Pre = "api/appemg/insertpre";
    private String Emg = "api/appemg/insert";
    private String Emg_Post = "api/appemg/insertpost";
    private String DetectionTimes = "api/appemg/recordtime";
    private String HistoricalRecord = "api/appemg/hisrecord";

    public String GetEmg_PreApi() {
        return Url + Emg_Pre;
    }

    public String GetEmgApi() {
        return Url + Emg;
    }

    public String GetEmg_PostApi() {
        return Url + Emg_Post;
    }

    public String GetDetectionTimesApi() {
        return Url + DetectionTimes;
    }

    public String GetHistoricalRecordApi() {
        return Url + HistoricalRecord;
    }
}
