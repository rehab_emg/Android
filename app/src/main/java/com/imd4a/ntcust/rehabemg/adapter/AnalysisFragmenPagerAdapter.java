package com.imd4a.ntcust.rehabemg.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class AnalysisFragmenPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> listFragments;

    public AnalysisFragmenPagerAdapter(FragmentManager fm, List<Fragment> mlistFragments){
        super(fm);
        this.listFragments = mlistFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }
}
