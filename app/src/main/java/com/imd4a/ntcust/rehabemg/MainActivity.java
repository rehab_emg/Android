package com.imd4a.ntcust.rehabemg;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.imd4a.ntcust.rehabemg.Fragment.DescriptionFragment;
import com.imd4a.ntcust.rehabemg.Fragment.DetectFragment;
import com.imd4a.ntcust.rehabemg.Fragment.HistoryFragment;
import com.imd4a.ntcust.rehabemg.Fragment.MainFragment;
import com.imd4a.ntcust.rehabemg.Fragment.UserFragment;
import com.imd4a.ntcust.rehabemg.Model.UserModel;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private FloatingActionButton fab;

    private String TAG = "MainActivity"; // Toast Tag
    public String userAccount, usertoken, userimg, useremail; // 使用者資料
    private SharedPreferences sharedPreferences; // 暫存檔設定
    public String account, name, email, cellphone, birthday;

    private MainFragment mainFragment;
    private UserFragment userFragment;
    private DetectFragment detectFragment;
    private DescriptionFragment descriptionFragment;
    private HistoryFragment historyFragment;

    private boolean mainFragmentIsShow = false;
    private boolean userFragmentIsShow = false;
    private boolean detectEMGFragmentIsShow = false;
    private boolean historyFragmentIsShow = false;
    private boolean descriptionFragmentIsShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 保持喚醒

        // 取得使用者資料
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        userAccount = sharedPreferences.getString("Account", "未登入");
        usertoken = sharedPreferences.getString("Token", "無token");
        userimg = sharedPreferences.getString("Img", "無圖片");
        useremail = sharedPreferences.getString("Email", "無Email");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // region FloatinActionButton
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 傳個人資料
                UserModel userModel = userFragment.userModel;

                Bundle bundle = new Bundle();
                bundle.putString("account", userModel.account);
                bundle.putString("name", userModel.name);
                bundle.putString("cellphone", userModel.cellphone);
                bundle.putString("email", userModel.email);

                Intent intent = new Intent(MainActivity.this, ModifyActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
        // endregion

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (navigationView.getHeaderCount() > 0) {
            // 設定nav bar top文字
            View headerView = navigationView.getHeaderView(0);
            // 設定大頭貼
//        userimg = "../../../../../../../../back-end_new/API/" + userimg;// Environment.getExternalStorageDirectory().getAbsolutePath()
//            File imgFile = new File(userimg);
//            if(imgFile.exists()){
//                ImageView img = headerView.findViewById(R.id.imageView);
//                img.setImageBitmap(getBitmapFromAssets(userimg));
//                img.setImageBitmap(BitmapFactory.decodeFile(userimg));
//                img.setImageURI(Uri.fromFile(imgFile));
//            }

            ImageView img = headerView.findViewById(R.id.imageView);
//            testimgpath = "/res/drawable/test02.jpg";
//            File imgFile = new File(testimgpath);
//            if(imgFile.exists()){;
//                img.setImageURI(Uri.fromFile(imgFile));
//            }
            img.setImageResource(R.drawable.test01);

            TextView tx1 = (TextView) headerView.findViewById(R.id.nav_top_Name);
            tx1.setText(userAccount);
            TextView tx2 = (TextView) headerView.findViewById(R.id.nav_top_Email);
            tx2.setText(useremail); //改信箱
        }

        // 載入首頁內容
        InitFragment();
        // 設定第一頁為 MainFragment 首頁
        navigationView.setCheckedItem(R.id.nav_home);
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();
        mainFragment = new MainFragment();
        frgTran.add(R.id.contentLayout, mainFragment, "MainFragment");
        frgTran.commit();
    }

//    public void getUserData() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Userinfo();
//            }
//        }).start();
//    }

    private Bitmap getBitmapFromAssets(String fileName) {
        AssetManager am = getAssets();
        InputStream is = null;
        try {
            is = am.open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mainFragmentIsShow) {
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    confirmExit();
                    return true;
                }
            } else {
                FragmentSwitch_home();
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    private void confirmExit() {
        final AlertDialog.Builder alrDlg = new AlertDialog.Builder(MainActivity.this);
        alrDlg.setTitle("訊息");
        alrDlg.setMessage("確定要離開嗎？");
        alrDlg.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        alrDlg.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alrDlg.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        switch (id) {
            case R.id.nav_home:
                if (!mainFragmentIsShow) {
                    FragmentSwitch_home();
                }
                break;
            case R.id.nav_user_data: // 個人資料
                if (!userFragmentIsShow) {
                    FragmentSwitch_user();
                }
                break;
            case R.id.nav_detect_emg: // 檢測肌電
                if (!detectEMGFragmentIsShow) {
                    FragmentSwitch_detect();
                }
                break;
            case R.id.nav_historical_record: // 歷史紀錄
                if (!historyFragmentIsShow) {
                    FragmentSwitch_history();
                }
                break;
            case R.id.nav_sign_out: // 登出
                Log.i(TAG, "登出");
                // Dialog
                sharedPreferences.edit().remove("Account").remove("Password").remove("isLogin").commit();
                Intent it = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(it);
                finish();
                break;
            case R.id.nav_description:
                if (!descriptionFragmentIsShow) {
                    FragmentSwitch_descripyion();
                }
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void FragmentSwitch_home() {
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();

        InitFragment();
        mainFragmentIsShow = true;

        Log.i(TAG, "切換Fragment 首頁");
        mainFragment = new MainFragment();

        frgTran.replace(R.id.contentLayout, mainFragment, "MainFragment");
        frgTran.commit();
        navigationView.setCheckedItem(R.id.nav_home);
        fab.setVisibility(View.INVISIBLE);
    }

    public void FragmentSwitch_user() {
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();

        InitFragment();
        userFragmentIsShow = true;
        Log.i(TAG, "切換Fragment 個人資料");
        userFragment = new UserFragment();
        frgTran.replace(R.id.contentLayout, userFragment, "UserFragment");
        frgTran.commit();
        navigationView.setCheckedItem(R.id.nav_user_data);
        fab.setVisibility(View.VISIBLE);
    }

    public void FragmentSwitch_detect() {
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();

        InitFragment();
        detectEMGFragmentIsShow = true;

        Log.i(TAG, "切換Fragment 檢測肌電");
        detectFragment = new DetectFragment();

        frgTran.replace(R.id.contentLayout, detectFragment, "DetectFragment");
        frgTran.commit();
        navigationView.setCheckedItem(R.id.nav_detect_emg);
        fab.setVisibility(View.INVISIBLE);
    }

    public void FragmentSwitch_history() {
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();

        InitFragment();
        historyFragmentIsShow = true;

        Log.i(TAG, "切換Fragment 歷史紀錄");
        historyFragment = new HistoryFragment();

        frgTran.replace(R.id.contentLayout, historyFragment, "HistoryFragment");
        frgTran.commit();
        navigationView.setCheckedItem(R.id.nav_historical_record);
        fab.setVisibility(View.INVISIBLE);
    }

    public void FragmentSwitch_descripyion() {
        FragmentTransaction frgTran = getSupportFragmentManager().beginTransaction();

        InitFragment();
        descriptionFragmentIsShow = true;

        descriptionFragment = new DescriptionFragment();

        frgTran.replace(R.id.contentLayout, descriptionFragment, "DescriptionFragment");
        frgTran.commit();
        navigationView.setCheckedItem(R.id.nav_description);
        fab.setVisibility(View.INVISIBLE);
    }

    private void InitFragment() {
        Log.i(TAG, "InitFragment");

        mainFragmentIsShow = false;
        userFragmentIsShow = false;
        detectEMGFragmentIsShow = false;
        historyFragmentIsShow = false;
        descriptionFragmentIsShow = false;
    }
}
