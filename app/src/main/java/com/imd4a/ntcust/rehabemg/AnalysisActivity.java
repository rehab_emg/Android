package com.imd4a.ntcust.rehabemg;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.imd4a.ntcust.rehabemg.Fragment.Analysis1DataFragment;
import com.imd4a.ntcust.rehabemg.Fragment.Analysis2SemgFragment;
import com.imd4a.ntcust.rehabemg.Fragment.Analysis3RmsFragment;
import com.imd4a.ntcust.rehabemg.Fragment.Analysis4MfFragment;
import com.imd4a.ntcust.rehabemg.Fragment.Analysis5MpfFragment;
import com.imd4a.ntcust.rehabemg.Model.EMGDataDetailModel;
import com.imd4a.ntcust.rehabemg.Model.HttpResultEMGData;
import com.imd4a.ntcust.rehabemg.adapter.AnalysisFragmenPagerAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.util.ArrayList;
import java.util.List;

public class AnalysisActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {

    private WebApi webApi = new WebApi();

    private int position_current = 0;

    private ViewPager viewPager;
    private TabHost tabHost;
    private TabWidget tabWidget;

    private String detection_id;
    private String analysisUrl = webApi.GetHistoricalRecordApi();

    private JSONArray analysisData_current;

    public EMGDataDetailModel Data_Current1, Data_Current2;

    private ArrayList<Fragment> listFragments;

    private Analysis1DataFragment analysis1DataFragment;
    private Analysis2SemgFragment analysis2SemgFragment;
    private Analysis3RmsFragment analysis3RmsFragment;
    private Analysis4MfFragment analysis4MfFragment;
//    private Analysis5MpfFragment analysis5MpfFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.analysis_info, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (position_current) {
            case 0:
                String msg1 = Data_Current1.part + "\n";
                String msg2 = Data_Current2.part + "\n";
                if(Data_Current1.avg_rms < 25){
                    msg1 += "以RMS分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current1.avg_rms < 50){
                    msg1 += "以RMS分數來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current1.avg_rms < 75){
                    msg1 += "以RMS分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current1.avg_rms < 100){
                    msg1 += "以RMS分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg1 += "以RMS分數來看，恭喜您是正常人！\n";
                }
                if(Data_Current1.avg_mf < 25){
                    msg1 += "以MF分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current1.avg_mf < 50){
                    msg1 += "以MF來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current1.avg_mf < 75){
                    msg1 += "以MF分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current1.avg_mf < 100){
                    msg1 += "以MF分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg1 += "以MF分數來看，恭喜您是正常人！\n";
                }
                if(Data_Current1.avg < 25){
                    msg1 += "以總體分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current1.avg < 50){
                    msg1 += "以總體分數來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current1.avg < 75){
                    msg1 += "以總體分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current1.avg < 100){
                    msg1 += "以總體分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg1 += "以總體分數來看，恭喜您是正常人！\n";
                }

                if(Data_Current2.avg_rms < 25){
                    msg2 += "以RMS分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current2.avg_rms < 50){
                    msg2 += "以RMS分數來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current2.avg_rms < 75){
                    msg2 += "以RMS分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current2.avg_rms < 100){
                    msg2 += "以RMS分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg2 += "以RMS分數來看，恭喜您是正常人！\n";
                }
                if(Data_Current2.avg_mf < 25){
                    msg2 += "以MF分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current2.avg_mf < 50){
                    msg2 += "以MF分數來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current2.avg_mf < 75){
                    msg2 += "以MF分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current2.avg_mf < 100){
                    msg2 += "以MF分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg2 += "以MF分數來看，恭喜您是正常人！\n";
                }

                if(Data_Current2.avg < 25){
                    msg2 += "以總體分數來看，肌肉萎縮程度大\n較接近重度萎縮患者\n";
                }else if(Data_Current2.avg < 50){
                    msg2 += "以總體分數來看，肌肉萎縮程度小\n較接近輕度萎縮患者\n";
                }else if(Data_Current2.avg < 75){
                    msg2 += "以總體分數來看，肌肉很有可能有萎縮情況\n";
                }else if(Data_Current2.avg < 100){
                    msg2 += "以總體分數來看，肌肉有可能有萎縮情況，不過可能性較小\n";
                }else{
                    msg2 += "以總體分數來看，恭喜您是正常人！\n";
                }

                new AlertDialog.Builder(AnalysisActivity.this)
                        .setMessage(msg1 + "\n" + msg2)
                        .show();

//                new AlertDialog.Builder(AnalysisActivity.this)
//                        .setMessage("0~50分為肌肉萎縮區間\r\n" +
//                                "分數越低，肌肉萎縮程度越大\n" +
//                                "分數越高，肌肉萎縮程度越小\n\n" +
//                                "51~99分為潛在肌肉萎縮區間\n" +
//                                "分數越低，肌肉萎縮的可能性越高\n" +
//                                "分數越高，肌肉萎縮可能性越低\n\n" +
//                                "100分為正常人")
//                        .show();
                break;
            case 1:
                new AlertDialog.Builder(AnalysisActivity.this)
                        .setMessage("原始肌電資料\r\n\n" +
                                "活動時施力越大，振幅越大\n\n" +
                                "圖可放大、移動")
                        .show();
                break;
            case 2:
                new AlertDialog.Builder(AnalysisActivity.this)
                        .setMessage("RMS愈大，表示肌肉愈用力\n" +
                                "RMS開始下降時，表示肌肉到疲勞極限\n\n" +
                                "肌電訊號振幅(amplitude)大小的指標，評估肌電訊號強度，即肌肉施力的強度\n" +
                                "肌肉疲勞初期，會有較大的肌電強度代償，以維持肌肉力量的表現，RMS會暫時增大\n" +
                                "到了疲勞的極限，肌電訊號將受到抑制，而RMS值隨著疲勞的發生而變小")
                        .show();
                break;
            case 3:
                new AlertDialog.Builder(AnalysisActivity.this)
                        .setMessage("MF下降時表示肌肉出現疲勞")
                        .show();
                break;
            case 4:
                new AlertDialog.Builder(AnalysisActivity.this)
                        .setMessage("MPF下降時表示肌肉出現疲勞")
                        .show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);

        Intent it = getIntent();
        Bundle bundle = it.getExtras();
        detection_id = bundle.getString("detection_id");
        Log.e("DETECTION_ID", detection_id);

        // Activity 基本設定
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 保持喚醒
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("個人分析");

        Data_Current1 = new EMGDataDetailModel();
        Data_Current2 = new EMGDataDetailModel();

        // 分頁
        initFragment();
        initViewPager();
        initTabHost();
        initTabWidget();
        getData();
    }

    public void getData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Analysisinfo();
            }
        }).start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && viewPager.getCurrentItem() == 0) {
            return super.onKeyDown(keyCode, event);
        } else {
            tabWidget.setCurrentTab(0);
            viewPager.setCurrentItem(0);
        }

        return true;
    }

    private FragmentManager fm = getSupportFragmentManager();

    // region 分頁
    private void initFragment() {
        analysis1DataFragment = new Analysis1DataFragment();
        analysis2SemgFragment = new Analysis2SemgFragment();
        analysis3RmsFragment = new Analysis3RmsFragment();
        analysis4MfFragment = new Analysis4MfFragment();
//        analysis5MpfFragment = new Analysis5MpfFragment();
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        listFragments = new ArrayList<Fragment>();
        listFragments.add(analysis1DataFragment);
        listFragments.add(analysis2SemgFragment);
        listFragments.add(analysis3RmsFragment);
        listFragments.add(analysis4MfFragment);
//        listFragments.add(analysis5MpfFragment);

        AnalysisFragmenPagerAdapter analysisFragmenPagerAdapter = new AnalysisFragmenPagerAdapter(getSupportFragmentManager(), listFragments);

        viewPager.setAdapter(analysisFragmenPagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    private void initTabHost() {
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

//        String[] tabNames = {"檢測資料", "原始肌電", "RMS", "MF", "MPF"};
        String[] tabNames = {"檢測資料", "原始肌電", "RMS", "MF"};

        for (int i = 0; i < tabNames.length; i++) {
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator(tabNames[i]);
            tabSpec.setContent(new FakeContent(getApplicationContext()));
            tabHost.addTab(tabSpec);
        }

        tabHost.setOnTabChangedListener(this);
    }

    private void initTabWidget() {
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        for (int i = 0; i < listFragments.size(); i++) {
            TextView tx = tabWidget.getChildTabViewAt(i).findViewById(android.R.id.title);
            tx.setTextSize(16);
        }
    }

    public class FakeContent implements TabHost.TabContentFactory {

        Context context;

        public FakeContent(Context context) {
            this.context = context;
        }

        @Override
        public View createTabContent(String tag) {

            View fakeView = new View(context);
            fakeView.setMinimumHeight(0);
            fakeView.setMinimumWidth(0);
            return fakeView;
        }
    }

    // viewPager Listener
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabHost.setCurrentTab(position);
        this.position_current = position;
        Log.e("position", String.valueOf(position_current));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    // tabHost Listener
    @Override
    public void onTabChanged(String tabId) {
        int selectedPage = tabHost.getCurrentTab();
        viewPager.setCurrentItem(selectedPage);
    }
    // endregion

    //  個人分析API
    public void Analysisinfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dt_id", detection_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(analysisUrl)
                .post(body)
                .build();
        try {
            final Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("resultJSONresultJSON", resultJSON);
                analysisData_current = new JSONArray(resultJSON);

                for (int i = 0; i < analysisData_current.length(); i++) {
                    Gson gson = new Gson();
                    HttpResultEMGData httpResultEMGData = gson.fromJson(analysisData_current.getJSONObject(i).toString(), HttpResultEMGData.class);

                    if (i == 0) {
                        Data_Current1.isLR = httpResultEMGData.getIsLR();
                        Data_Current1.place = httpResultEMGData.getPlace();
                        Data_Current1.date = httpResultEMGData.getDate();
                        Data_Current1.part = httpResultEMGData.getPart();
                        Data_Current1.rms = httpResultEMGData.getRms();
                        Data_Current1.mf = httpResultEMGData.getMf();
                        Data_Current1.mpf = httpResultEMGData.getMpf();
                        Data_Current1.semg = httpResultEMGData.getData();
                        Data_Current1.avg_rms = httpResultEMGData.getAvg_rms();
                        Data_Current1.avg_mf = httpResultEMGData.getAvg_mf();
                        Data_Current1.avg = httpResultEMGData.getAvg();
                    } else {
                        Data_Current2.isLR = httpResultEMGData.getIsLR();
                        Data_Current2.place = httpResultEMGData.getPlace();
                        Data_Current2.date = httpResultEMGData.getDate();
                        Data_Current2.part = httpResultEMGData.getPart();
                        Data_Current2.rms = httpResultEMGData.getRms();
                        Data_Current2.mf = httpResultEMGData.getMf();
                        Data_Current2.mpf = httpResultEMGData.getMpf();
                        Data_Current2.semg = httpResultEMGData.getData();
                        Data_Current2.avg_rms = httpResultEMGData.getAvg_rms();
                        Data_Current2.avg_mf = httpResultEMGData.getAvg_mf();
                        Data_Current2.avg = httpResultEMGData.getAvg();
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        char[] date = Data_Current1.date.toCharArray();
                        char[] newDate = new char[date.length];
                        int reg = -1;
                        for (int i = 0; i < Data_Current1.date.length(); i++) {
                            if (date[i] == 'T') {
                                date[i] = ' ';
                            }
                            if (date[i] == '.') {
                                reg = i;
                            }
                            if (i == reg) {
                                break;
                            }
                            newDate[i] = date[i];
                        }
                        Data_Current1.date = String.valueOf(newDate);

                        analysis1DataFragment.setView(Data_Current1.isLR, Data_Current1.place, Data_Current1.date, Data_Current1.part, Data_Current1.avg_rms, Data_Current1.avg_mf, Data_Current1.avg, Data_Current2.part, Data_Current2.avg_rms, Data_Current2.avg_mf, Data_Current2.avg);
                        analysis2SemgFragment.setView(Data_Current1.isLR, Data_Current1.place, Data_Current1.date, Data_Current1.part, Data_Current1.semg, Data_Current2.part, Data_Current2.semg);
                    }
                });
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public Bundle get_1() {
        if (Data_Current2.avg != -1) {
            Bundle b = new Bundle();

            b.putString("isLR_place", Data_Current1.isLR + " " + Data_Current2.place);
            b.putString("date", Data_Current1.date);
            b.putString("part1", Data_Current1.part);
            b.putString("grade1", String.valueOf(Data_Current1.avg_rms));
            b.putString("part2", Data_Current2.part);
            b.putString("grade2", String.valueOf(Data_Current2.avg_rms));

            return b;
        }
        return null;
    }

    public Bundle get_2() {
        if (Data_Current2.avg != -1) {
            Bundle b = new Bundle();

            b.putString("part1", Data_Current1.part);
            b.putIntegerArrayList("semg1", Data_Current1.semg);
            b.putString("part2", Data_Current2.part);
            b.putIntegerArrayList("semg2", Data_Current2.semg);
            b.putString("isLR_place", Data_Current1.isLR + " " + Data_Current1.place);
            b.putString("date", Data_Current1.date);

            return b;
        }
        return null;
    }

    public Bundle get_3() {
        if (Data_Current2.avg != -1) {
            Bundle b = new Bundle();

            b.putString("part1", Data_Current1.part);
            b.putFloatArray("rms1", Data_Current1.rms);
            b.putString("part2", Data_Current2.part);
            b.putFloatArray("rms2", Data_Current2.rms);
            b.putString("isLR_place", Data_Current1.isLR + " " + Data_Current1.place);
            b.putString("date", Data_Current1.date);

            return b;
        }
        return null;
    }

    public Bundle get_4() {
        if (Data_Current2.avg != -1) {
            Bundle b = new Bundle();

            b.putString("part1", Data_Current1.part);
            b.putFloatArray("mf1", Data_Current1.mf);
            b.putString("part2", Data_Current2.part);
            b.putFloatArray("mf2", Data_Current2.mf);
            b.putString("isLR_place", Data_Current1.isLR + " " + Data_Current1.place);
            b.putString("date", Data_Current1.date);

            return b;
        }
        return null;
    }

    public Bundle get_5() {
        if (Data_Current2.avg != -1) {
            Bundle b = new Bundle();

            b.putString("part1", Data_Current1.part);
            b.putFloatArray("mpf1", Data_Current1.mpf);
            b.putString("part2", Data_Current2.part);
            b.putFloatArray("mpf2", Data_Current2.mpf);
            b.putString("isLR_place", Data_Current1.isLR + " " + Data_Current1.place);
            b.putString("date", Data_Current1.date);

            return b;
        }
        return null;
    }
}
