package com.imd4a.ntcust.rehabemg.Fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.imd4a.ntcust.rehabemg.AnalysisActivity;
import com.imd4a.ntcust.rehabemg.MainActivity;
import com.imd4a.ntcust.rehabemg.Model.EMGDataDetailModel;
import com.imd4a.ntcust.rehabemg.Model.HttpResultUserData;
import com.imd4a.ntcust.rehabemg.ModifyActivity;
import com.imd4a.ntcust.rehabemg.R;
import com.imd4a.ntcust.rehabemg.RegisterActivity;
import com.imd4a.ntcust.rehabemg.Model.UserModel;
import com.imd4a.ntcust.rehabemg.WebApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;
import static android.support.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserFragment extends Fragment {
    public UserModel userModel;

    private WebApi webApi = new WebApi();
    private String userUrl = webApi.GetUserInfoApi();

    private ProgressBar progressBar;
    private LinearLayout linearLayout;

    private static EditText edtaccount, edtname, edtemail, edtcellphone, edtbirthday;
    private static RadioGroup sexradioGroup;
    private static RadioButton radioButtonBoy, radioButtonGril;
    private ImageView img;

    public UserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        userModel = new UserModel();

        InitView();
        img.setImageResource(R.drawable.test01);
//        ((MainActivity) getActivity()).getUserData();

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("個人資料");

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Userinfo();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }).start();
    }

    private  void InitView() {
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar2);
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.linearLayout);

        img = getActivity().findViewById(R.id.img);
        edtaccount = (EditText) getActivity().findViewById(R.id.edt_account);
        edtname = (EditText) getActivity().findViewById(R.id.edt_name);
        edtemail = (EditText) getActivity().findViewById(R.id.edt_email);
        edtcellphone = (EditText) getActivity().findViewById(R.id.edt_cellphone);
        edtbirthday = (EditText) getActivity().findViewById(R.id.edt_birthday);
        sexradioGroup = (RadioGroup) getActivity().findViewById(R.id.sexradioGroup);
        radioButtonBoy = (RadioButton) getActivity().findViewById(R.id.radioButtonBoy);
        radioButtonGril = (RadioButton) getActivity().findViewById(R.id.radioButtonGril);
    }

    public void setUserView() {
        edtaccount.setText(userModel.account);
        edtbirthday.setText(userModel.birthday);
        edtcellphone.setText(userModel.cellphone);
        edtname.setText(userModel.name);
        edtemail.setText(userModel.email);
        switch (userModel.sex){
            case 1:
                radioButtonBoy.setChecked(true);
                break;
            case 2:
                radioButtonGril.setChecked(true);
                break;
            default:
                break;
        }
        progressBar.setVisibility(View.INVISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
    }

    //  個人資料API
    public void Userinfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", ((MainActivity) getActivity()).userAccount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final OkHttpClient client = new OkHttpClient();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(userUrl)
                .post(body)
                .build();
//                .addHeader("Authorization", usertoken)
        try {
            final Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resultJSON = null;
                try {
                    resultJSON = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                HttpResultUserData result = gson.fromJson(resultJSON, HttpResultUserData.class);
                userModel.account = result.getAccount();
                userModel.name = result.getName();
                userModel.email = result.getEmail();
                userModel.sex = result.getSex();
                userModel.birthday = result.getBirthday().substring(0, 10);
                userModel.cellphone = result.getCellphone();
                userModel.img = result.getImg();

                ((MainActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setUserView();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
